CREATE TABLE IF NOT EXISTS users(
	id INTEGER PRIMARY KEY,
	username TEXT NOT NULL,
	password TEXT NOT NULL
);

INSERT INTO users (username, password) VALUES ("root", "lol");

CREATE TABLE IF NOT EXISTS equipments(
	id INTEGER PRIMARY KEY,
	unique_id CHAR(16) DEFAULT(lower(hex(randomblob(4)))),
	name TEXT NOT NULL,
	status BOOLEAN DEFAULT(0)
);

CREATE TABLE IF NOT EXISTS equipments_drivers(
	equipment INTEGER,
	driver TEXT NOT NULL,
	FOREIGN KEY(equipment) REFERENCES equipments(id)
);

CREATE TABLE IF NOT EXISTS equipments_connectors(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	equipment INTEGER,
	connector TEXT NOT NULL,
	FOREIGN KEY(equipment) REFERENCES equipments(id)
);

CREATE TABLE IF NOT EXISTS equipments_connectors_params(
	equipment_connector INTEGER,
	key TEXT NOT NULL,
	value TEXT NOT NULL,
	FOREIGN KEY(equipment_connector) REFERENCES equipments_connectors(id)
);
