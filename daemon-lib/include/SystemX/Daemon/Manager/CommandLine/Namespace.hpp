#ifndef DAEMON_MANAGER_COMMANDLINE_NAMESPACE_HPP
#define DAEMON_MANAGER_COMMANDLINE_NAMESPACE_HPP

#include "../Network/TelnetClient.hpp"

#include <vector>
#include <functional>
#include <map>
#include <string>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				class TelnetClient;
			}

			namespace CommandLine
			{
				struct Command
				{
					std::string name;
					std::string description;
					std::function<void(Network::TelnetClient*, std::vector<std::string>)> function;
					std::function<std::string(std::vector<std::string>)> auto_completion;
				};

				class Namespace
				{
				public:
					Namespace(Namespace *parent, std::string name, std::string description);
					Namespace(Namespace *parent, std::string name);
					Namespace(Namespace *parent);
					virtual ~Namespace();

					Namespace *Parent();
					void SetParent(Namespace *parent);
					std::string Name();
					std::string Description();
					void UnregisterCommand(std::string name);
					void RegisterCommand(std::string name, std::string description, std::function<void(Network::TelnetClient*, std::vector<std::string>)> function, std::function<std::string(std::vector<std::string>)> auto_completion = nullptr);
					Namespace *RegisterNamespace(std::string name, std::string description);
					void RegisterNamespace(Namespace *name_space);
					bool ExistCommand(std::string name);
					bool ExistNamespace(std::string name);
					std::function<void(Network::TelnetClient*, std::vector<std::string>)> GetCommand(std::string name);
					Namespace *GetNamespace(std::string name);
					std::map<std::string, std::string> List();
					std::string AutoComplete(std::string text);
				protected:
					Namespace *m_parent;
					std::string m_name;
					std::string m_description;
					std::map<std::string, Command> m_commands;
					std::map<std::string, Namespace*> m_namespaces;

					void Help(Network::TelnetClient *client, std::vector<std::string> args);
					virtual void Exit(Network::TelnetClient *client, std::vector<std::string> args);
				};
			}
		}
	}
}

#endif
