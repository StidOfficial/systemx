#ifndef DAEMON_MANAGER_EQUIPMENT_HPP
#define DAEMON_MANAGER_EQUIPMENT_HPP

#include <SystemX/Equipment/Equipment.hpp>
#include <SystemX/Daemon/Manager/Equipment/Drivers.hpp>
#include <SystemX/Daemon/Manager/Equipment/Connectors.hpp>

#include <string>
#include <memory>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			class Connector;

			namespace Equipment
			{
				class Drivers;
				class Connectors;

				class Equipment : public SystemX::Equipment::Equipment
				{
				public:
					Equipment(int id, std::string uniqueId, std::string name, bool status);
					virtual ~Equipment();

					int Id();
					std::string UniqueId();
					void SetName(std::string name);
					std::string GetName();
					void SetType(SystemX::Equipment::Type type);
					SystemX::Equipment::Type GetType();
					void SetDescription(std::string description);
					std::string GetDescription();
					void SetStatus(bool status);
					bool GetStatus();
					Connectors *GetConnectors();
				private:
					int m_id;
					std::string m_uniqueId;
					std::string m_name;
					SystemX::Equipment::Type m_type;
					std::string m_description;
					bool m_status;
					std::unique_ptr<Connectors> m_connectors;
				};
			}
		}
	}
}

#endif
