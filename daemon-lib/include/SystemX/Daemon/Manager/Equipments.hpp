#ifndef DAEMON_MANAGER_EQUIPMENTS_HPP
#define DAEMON_MANAGER_EQUIPMENTS_HPP

#include <SystemX/Daemon/Manager/Network/TelnetClient.hpp>
#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

#include <vector>
#include <string>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			class Equipments
			{
			public:
				static void Initialize();
				static bool Exist(std::string uniqueId);
				static Equipment::Equipment *Get(std::string uniqueId);
				static Equipment::Equipment *Add(std::string name);
				static void Remove(std::string name);
				static std::map<std::string, Equipment::Equipment*> All();
				static void Destroy();
			private:
				static std::map<std::string, Equipment::Equipment*> m_equipments;
			};
		}
	}
}

#endif
