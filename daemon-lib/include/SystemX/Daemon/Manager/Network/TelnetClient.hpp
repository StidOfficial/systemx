#ifndef DAEMON_MANAGER_NETWORK_TELNETCLIENT_HPP
#define DAEMON_MANAGER_NETWORK_TELNETCLIENT_HPP

#include "../CommandLine/Interface.hpp"

#include <SystemX/Network/TelnetClient.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				class Interface;
			}

			namespace Network
			{
				class TelnetClient : public SystemX::Network::TelnetClient
				{
				public:
					TelnetClient(int socket, SystemX::Network::InetAddress *address, SystemX::Network::SocketServer<SystemX::Network::Socket> *server);
					~TelnetClient();

					virtual void ReceiveLoop();
					std::string ReadLine();
					std::string ReadLine(bool password);
					std::string ReadPassword();
					std::string ReadCommand();
					CommandLine::Interface *CommandInterface();
				private:
					CommandLine::Interface *m_interface;
				};
			}
		}
	}
}

#endif
