#include <SystemX/Daemon/Manager/Connector.hpp>

#include <SystemX/Logger.hpp>

#include <dlfcn.h>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			Connector::Connector(std::string name, std::string version)
				: m_name(name), m_version(version)
			{
			}

			void Connector::Initialize()
			{
			}

			void Connector::FirstInitialize(Equipment::Equipment */*equipment*/, const std::map<std::string, std::string> /*params*/)
			{
			}

			void Connector::Initialize(Equipment::Equipment */*equipment*/, const std::map<std::string, std::string> /*params*/)
			{
			}

			void Connector::Uninitialize(Equipment::Equipment */*equipment*/)
			{
			}

			void Connector::Uninitialize()
			{
			}

			void Connector::Add(Equipment::Equipment *equipment, const std::map<std::string, std::string> params)
			{
				Initialize(equipment, params);
				m_equipments.insert(m_equipments.begin(), equipment);
			}

			void Connector::Remove(Equipment::Equipment *equipment)
			{
				for(size_t i = 0; i < m_equipments.size(); i++)
					if(m_equipments[i] == equipment)
					{
						Uninitialize(equipment);
						m_equipments.erase(m_equipments.begin() + i);
						break;
					}
			}

			std::string Connector::Name()
			{
				return m_name;
			}

			std::string Connector::Version()
			{
				return m_version;
			}

			Connector::~Connector()
			{
			}
		}
	}
}
