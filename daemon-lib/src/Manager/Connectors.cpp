#include <SystemX/Daemon/Manager/Connectors.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			std::map<std::string, Connector*> Connectors::m_connectors;

			void Connectors::Initialize()
			{
				for(const auto& connector : m_connectors)
					connector.second->Initialize();
			}

			void Connectors::Register(Connector *connector)
			{
				m_connectors.insert(std::pair<std::string, Connector*>(connector->Name(), connector));
			}

			std::map<std::string, Connector*> Connectors::All()
			{
				return m_connectors;
			}

			Connector *Connectors::Get(std::string name)
			{
				for(const auto& connector : m_connectors)
					if(connector.second->Name() == name)
						return connector.second;

				return nullptr;
			}

			void Connectors::Destroy()
			{
			}
		}
	}
}
