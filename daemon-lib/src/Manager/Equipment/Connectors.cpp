#include <SystemX/Daemon/Manager/Equipment/Connectors.hpp>

#include <SystemX/Daemon/Manager/Manager.hpp>
#include <SystemX/Daemon/Manager/Connectors.hpp>

#include <algorithm>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Equipment
			{
				Connectors::Connectors(Equipment *equipment)
					: m_equipment(equipment)
				{
					Database::DatabaseStatement *statement = Manager::Database()->Prepare("SELECT id, connector FROM equipments_connectors WHERE equipment = :id;");
					statement->Bind(":id", m_equipment->Id());
					while(statement->Next())
					{
						Connector *connector = Daemon::Manager::Connectors::Get(statement->GetString("connector"));
						if(connector)
						{
							Database::DatabaseStatement *paramsStatement = Manager::Manager::Database()->Prepare("SELECT key, value FROM equipments_connectors_params WHERE equipment_connector = :id;");
							paramsStatement->Bind(":id", statement->GetInt("id"));

							std::map<std::string, std::string> params;
							while(paramsStatement->Next())
								params.emplace(paramsStatement->GetString("key"), paramsStatement->GetString("value"));
							delete paramsStatement;

							m_connectors.push_back(connector);
							connector->Add(m_equipment, params);
						}
						else
							Logger::Warn("%s connector is not loaded !", statement->GetString("connector").c_str());
					}

					delete statement;
				}

				bool Connectors::Exist(Connector *connector)
				{
					auto it = std::find(m_connectors.begin(), m_connectors.end(), connector);

					return it != m_connectors.end();
				}

				void Connectors::Add(Connector *connector, std::map<std::string, std::string> params)
				{
					m_connectors.push_back(connector);
					connector->Add(m_equipment, params);

					connector->FirstInitialize(m_equipment, params);

					Database::DatabaseStatement *statement = Manager::Database()->Prepare("INSERT INTO equipments_connectors(equipment, connector) VALUES(:id, :connector);");
					statement->Bind(":id", m_equipment->Id());
					statement->Bind(":connector", connector->Name());
					statement->Execute();

					delete statement;

					int equipmentConnectorId = Manager::Database()->LastInsertRowID();
					for(auto &param : params)
					{
						Database::DatabaseStatement *statementParams = Manager::Database()->Prepare("INSERT INTO equipments_connectors_params(equipment_connector, key, value) VALUES(:equipmentConnector, :key, :value);");
						statementParams->Bind(":equipmentConnector", equipmentConnectorId);
						statementParams->Bind(":key", param.first);
						statementParams->Bind(":value", param.second);
						statementParams->Execute();
						delete statementParams;
					}
				}

				void Connectors::Remove(Connector *connector)
				{
					auto it = std::find(m_connectors.begin(), m_connectors.end(), connector);

					if(it != m_connectors.end())
						m_connectors.erase(it);

					Database::DatabaseStatement *statement = Manager::Database()->Prepare("DELETE FROM equipments_connectors WHERE equipment = :id AND connector = :connector;");
					statement->Bind(":id", m_equipment->Id());
					statement->Bind(":connector", connector->Name());
					statement->Execute();
					delete statement;

					Manager::Database()->Exec("DELETE FROM equipments_connectors_params WHERE equipment_connector NOT IN (SELECT DISTINCT id FROM equipments_connectors);");
				}

				std::vector<Connector*>::iterator Connectors::begin()
				{
					return m_connectors.begin();
				}

				std::vector<Connector*>::iterator Connectors::end()
				{
					return m_connectors.end();
				}
			}
		}
	}
}
