#include <SystemX/Daemon/Manager/Equipments.hpp>

#include <SystemX/Daemon/Manager/Manager.hpp>
#include <SystemX/Database/DatabaseStatement.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			std::map<std::string, Equipment::Equipment*> Equipments::m_equipments;

			void Equipments::Initialize()
			{
				Database::DatabaseStatement *equipmentStatement = Manager::Database()->Prepare("SELECT id, unique_id, name, status FROM equipments;");
				while(equipmentStatement->Next())
				{
					Equipment::Equipment *equipment = new Equipment::Equipment(
						equipmentStatement->GetInt("id"),
						equipmentStatement->GetString("unique_id"),
						equipmentStatement->GetString("name"),
						static_cast<bool>(equipmentStatement->GetInt("status"))
					);

					m_equipments.insert(std::pair<std::string, Equipment::Equipment*>(equipmentStatement->GetString("unique_id"), equipment));
				}

				delete equipmentStatement;
			}

			bool Equipments::Exist(std::string uniqueId)
			{
				return Get(uniqueId);
			}

			Equipment::Equipment *Equipments::Get(std::string uniqueId)
			{
				auto it = m_equipments.find(uniqueId);
				if(it != m_equipments.end())
					return it->second;

				return nullptr;
			}

			Equipment::Equipment *Equipments::Add(std::string name)
			{
				Database::DatabaseStatement *statement;

				statement = Manager::Database()->Prepare("INSERT INTO equipments(name) VALUES (:name);");
				statement->Bind(":name", name);
				statement->Execute();
				delete statement;

				statement = Manager::Database()->Prepare("SELECT unique_id FROM equipments WHERE id = :id;");
				statement->Bind(":id", Manager::Database()->LastInsertRowID());
				statement->Execute();

				Equipment::Equipment *equipment = new Equipment::Equipment(
					Manager::Database()->LastInsertRowID(),
					statement->GetString("unique_id"),
					name,
					0
				);
				delete statement;

				m_equipments.insert(std::pair<std::string, Equipment::Equipment*>(equipment->UniqueId(), equipment));

				return equipment;
			}

			void Equipments::Remove(std::string uniqueId)
			{
				auto it = m_equipments.find(uniqueId);
				if(it == m_equipments.end())
					return;

				Equipment::Equipment *equipment = Get(uniqueId);

				Database::DatabaseStatement *statement = Manager::Database()->Prepare("DELETE FROM equipments WHERE unique_id = :unique_id;");
				statement->Bind(":unique_id", uniqueId);
				statement->Execute();

				delete equipment;
				m_equipments.erase(it);
			}

			std::map<std::string, Equipment::Equipment*> Equipments::All()
			{
				return m_equipments;
			}

			void Equipments::Destroy()
			{
				for(auto &equipment : m_equipments)
					delete equipment.second;
			}
		}
	}
}
