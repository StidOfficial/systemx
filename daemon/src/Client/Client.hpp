#ifndef DAEMON_CLIENT_CLIENT_HPP
#define DAEMON_CLIENT_CLIENT_HPP

//#include <SystemX/Daemon/Network/SystemXServer.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Client
		{
			class Client
			{
			public:
				static void Initialize();
				static void Run();
				static void Shutdown();
				static void Destroy();
			private:
				//static SystemX::Network::SystemXServer *m_systemx_server;
			};
		}
	}
}

#endif
