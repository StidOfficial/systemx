#include "Drivers.hpp"

#include <SystemX/Drivers.hpp>
#include <SystemX/Utils.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{

				Drivers::Drivers()
					: Namespace(nullptr, "drivers", "Drivers configuration")
				{
					RegisterCommand("show", "Show drivers", std::bind(&Drivers::Show, this, std::placeholders::_1, std::placeholders::_2));
				}

				void Drivers::Show(Network::TelnetClient *client, std::vector<std::string> /*args*/)
				{
					client->Send("\r\nName\t\tVersion");
					client->Send("\r\n---------------------------");

					for(auto &driver : SystemX::Drivers::All())
						client->Send("\r\n" + SystemX::Utils::TableFill(driver.second->Name(), 8) + driver.second->Version());
				    }

				Drivers::~Drivers()
				{
				}
			}
		}
	}
}
