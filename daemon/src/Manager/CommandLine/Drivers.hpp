#ifndef DAEMON_MANAGER_COMMANDLINE_DRIVERS_HPP
#define DAEMON_MANAGER_COMMANDLINE_DRIVERS_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				class Drivers : public Namespace
				{
				public:
					Drivers();
					virtual ~Drivers();
				private:
					void Show(Network::TelnetClient *client, std::vector<std::string> args);
				};
			}
		}
	}
}

#endif
