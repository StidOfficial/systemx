#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_CONNECTORS_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_CONNECTORS_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>
#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					class Connectors : public Namespace
					{
					public:
						Connectors(Namespace *base_namespace, Manager::Equipment::Equipment *equipment);
						virtual ~Connectors();
					private:
						Manager::Equipment::Equipment *m_equipment;

						void Remove(Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Add(Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Show(Manager::Network::TelnetClient *client, std::vector<std::string> args);
						virtual void Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args);
					};
				}
			}
		}
	}
}

#endif
