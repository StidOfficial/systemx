#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_DRIVERS_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_DRIVERS_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>
#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					class Drivers : public Namespace
					{
					public:
						Drivers(Namespace *base_namespace, Manager::Equipment::Equipment *equipment);
						virtual ~Drivers();
					private:
						Manager::Equipment::Equipment *m_equipment;

						void Add(Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Remove(Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Show(Manager::Network::TelnetClient *client, std::vector<std::string> args);
						virtual void Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args);
					};
				}
			}
		}
	}
}

#endif
