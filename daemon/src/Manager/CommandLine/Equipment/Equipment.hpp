#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_EQUIPMENT_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_EQUIPMENT_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>
#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					class Equipment : public Namespace
					{
					public:
						Equipment(Namespace *base_namespace, Daemon::Manager::Equipment::Equipment *equipment);
						virtual ~Equipment();
					private:
						Manager::Equipment::Equipment *m_equipment;

						void Id(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Name(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void MemInfo(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Power(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Network(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Drivers(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						void Connectors(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
						virtual void Exit(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args);
					};
				}
			}
		}
	}
}

#endif
