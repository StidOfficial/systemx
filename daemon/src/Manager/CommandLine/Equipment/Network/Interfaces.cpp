#include "Interfaces.hpp"

#include "Interface.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					namespace Network
					{
						Interfaces::Interfaces(Namespace *base_namespace, Manager::Equipment::Equipment *equipment)
							: Namespace(base_namespace, "interfaces"), m_equipment(equipment)
						{
							RegisterCommand("show", "Show network interfaces", std::bind(&Interfaces::Show, this, std::placeholders::_1, std::placeholders::_2));
							RegisterCommand("interface", "Configure interface", std::bind(&Interfaces::Interface, this, std::placeholders::_1, std::placeholders::_2), std::bind(&Interfaces::AutoCompleteInterface, this, std::placeholders::_1));
						}

						void Interfaces::Show(Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
						{
							client->Send("\r\nName\t\tVersion");
							client->Send("\r\n--------------------------");

							for(auto &interface : *m_equipment->Interfaces())
								client->Send("\r\n" + interface->GetName());
						}

						void Interfaces::Interface(Manager::Network::TelnetClient *client, std::vector<std::string> args)
						{
							if(args.size() < 1)
							{
								client->Send("\r\nNo name interface specified !");
								return;
							}

							SystemX::Equipment::Network::Interface *interface = m_equipment->Interfaces()->Get(args[0]);
							if(interface)
							{
								CommandLine::Namespace *interfaceNamespace = new CommandLine::Equipment::Network::Interface(this, interface);
								client->CommandInterface()->ChangeNamespace(interfaceNamespace);
							}
							else
								client->Send("\r\nNo interface found !");
						}

						std::string Interfaces::AutoCompleteInterface(std::vector<std::string> args)
						{
							if(args.size() == 1 && !args[0].empty())
								for(auto &interface : *m_equipment->Interfaces())
									if(interface->GetName().compare(0, args[0].length(), args[0]) == 0)
										return interface->GetName();

							return std::string();
						}

						void Interfaces::Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args)
						{
							Namespace::Exit(client, args);
							delete this;
						}

						Interfaces::~Interfaces()
						{
						}
					}
				}
			}
		}
	}
}
