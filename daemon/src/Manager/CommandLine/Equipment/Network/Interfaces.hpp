#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_NETWORK_INTERFACE_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_NETWORK_INTERFACE_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>
#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>
#include <SystemX/Daemon/Manager/Network/TelnetClient.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
          namespace Network
          {
  					class Interfaces : public Namespace
  					{
  					public:
  						Interfaces(Namespace *base_namespace, Manager::Equipment::Equipment *equipment);
  						virtual ~Interfaces();
  					private:
  						Manager::Equipment::Equipment *m_equipment;

  						void Show(Manager::Network::TelnetClient *client, std::vector<std::string> args);
							void Interface(Manager::Network::TelnetClient *client, std::vector<std::string> args);
							std::string AutoCompleteInterface(std::vector<std::string> args);
  						virtual void Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args);
  					};
          }
				}
			}
		}
	}
}

#endif
