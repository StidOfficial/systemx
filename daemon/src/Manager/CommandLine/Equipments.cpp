#include "Equipments.hpp"

#include "Equipment/Equipment.hpp"
#include <SystemX/Daemon/Manager/Equipments.hpp>

#include <SystemX/Utils.hpp>

#include <SystemX/Logger.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				Equipments::Equipments()
					: Namespace(nullptr, "equipments", "Equipments configuration")
				{
					RegisterCommand("show", "Show equipments", std::bind(&Equipments::Show, this, std::placeholders::_1, std::placeholders::_2));
					RegisterCommand("remove", "Remove equipment", std::bind(&Equipments::Remove, this, std::placeholders::_1, std::placeholders::_2));
					RegisterCommand("add", "Add equipment", std::bind(&Equipments::Add, this, std::placeholders::_1, std::placeholders::_2));
					RegisterCommand("equipment", "Configure equipment", std::bind(&Equipments::Equipment, this, std::placeholders::_1, std::placeholders::_2), std::bind(&Equipments::AutoCompleteEquipment, this, std::placeholders::_1));
				}

				void Equipments::Show(Network::TelnetClient *client, std::vector<std::string> /*args*/)
				{
					client->Send("\r\nID\t\tName\t\tStatus");
					client->Send("\r\n------------------------------------------------------");

					for(auto &equipment : Manager::Equipments::All())
						client->Send("\r\n" + Utils::TableFill(equipment.second->UniqueId(), 8) + Utils::TableFill(equipment.second->GetName(), 8) + (equipment.second->GetStatus() ? "Online" : "Offline"));
				}

				void Equipments::Remove(Network::TelnetClient *client, std::vector<std::string> args)
				{
					if(args.size() < 1)
					{
						client->Send("\r\nNo id equipment specified !");
						return;
					}

					if(Manager::Equipments::Exist(args[0]))
						Manager::Equipments::Remove(args[0]);
					else
						client->Send("\r\nEquipment not found");
				}

				void Equipments::Add(Network::TelnetClient *client, std::vector<std::string> args)
				{
					if(args.size() >= 1)
					{
						Manager::Equipment::Equipment *equipment = Manager::Equipments::Add(args[0]);
						client->Send("\r\n" + equipment->UniqueId());
					}
					else
						client->Send("\r\nNo name specified !");
				}

				void Equipments::Equipment(Network::TelnetClient *client, std::vector<std::string> args)
				{
					if(args.size() < 1)
					{
						client->Send("\r\nNo id equipment specified !");
						return;
					}

					Manager::Equipment::Equipment *equipment = Manager::Equipments::Get(args[0]);
					if(equipment)
					{
						CommandLine::Namespace *equipmentNamespace = new Equipment::Equipment(this, equipment);
						client->CommandInterface()->ChangeNamespace(equipmentNamespace);
					}
					else
						client->Send("\r\nNo equipment found !");
				}

				std::string Equipments::AutoCompleteEquipment(std::vector<std::string> args)
				{
					if(args.size() == 1 && !args[0].empty())
						for(auto &equipment : Manager::Equipments::All())
							if(equipment.first.compare(0, args[0].length(), args[0]) == 0)
								return equipment.first;

					return std::string();
				}

				Equipments::~Equipments()
				{
				}
			}
		}
	}
}
