#include <SystemX/Daemon/Manager/CommandLine/Interface.hpp>

#include <SystemX/Daemon/Manager/Network/TelnetClient.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				std::vector<std::function<void(Namespace*)>> Interface::m_initializer;

				Interface::Interface()
				{
					m_base = new Namespace(nullptr, "", "");
					m_base->UnregisterCommand("exit");
					for(const auto& initialize : m_initializer)
						initialize(m_base);

					m_current = m_base;
				}

				void Interface::ChangeNamespace(Namespace *name_space)
				{
					m_current = name_space;
				}

				Namespace *Interface::CurrentNamespace()
				{
					return m_current;
				}

				std::string Interface::Path()
				{
					std::string path = "/";
					for(Namespace *name_space = CurrentNamespace(); name_space != nullptr; name_space = name_space->Parent())
						if(!name_space->Name().empty())
							path.insert(0, "/" + name_space->Name());

					return path;
				}

				Namespace* Interface::Base()
				{
					return m_base;
				}

				void Interface::Register(std::function<void(Namespace*)> func)
				{
					m_initializer.push_back(func);
				}

				void Interface::Logout(Network::TelnetClient *client, std::vector<std::string> /*args*/)
				{
					client->Send("\r\nGood bye !\r\n");
					client->SetOption(SystemX::Network::TelnetCommand::DO, SystemX::Network::TelnetOption::Logout);
					client->SendOption();
					client->Close();
				}

				Interface::~Interface()
				{
					delete m_base;
				}
			}
		}
	}
}
