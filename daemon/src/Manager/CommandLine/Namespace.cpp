#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>

#include <SystemX/Daemon/Manager/CommandLine/Interface.hpp>

#include <SystemX/Logger.hpp>
#include <SystemX/Utils.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				Namespace::Namespace(Namespace *parent, std::string name, std::string description)
					: m_parent(parent), m_name(name), m_description(description)
				{
					RegisterCommand("?", "Show help", std::bind(&Namespace::Help, this, std::placeholders::_1, std::placeholders::_2));
					RegisterCommand("help", "Show help", std::bind(&Namespace::Help, this, std::placeholders::_1, std::placeholders::_2));
					RegisterCommand("exit", "Exit namespace", std::bind(&Namespace::Exit, this, std::placeholders::_1, std::placeholders::_2));
					RegisterCommand("logout", "Logout session", Interface::Logout);
				}

				Namespace::Namespace(Namespace *parent, std::string name)
					: Namespace(parent, name, "")
				{
				}

				Namespace::Namespace(Namespace *parent)
				: Namespace(parent, "")
				{
				}

				Namespace *Namespace::Parent()
				{
					return m_parent;
				}

				void Namespace::SetParent(Namespace *parent)
				{
					m_parent = parent;
				}

				std::string Namespace::Name()
				{
					return m_name;
				}

				std::string Namespace::Description()
				{
					return m_description;
				}

				void Namespace::UnregisterCommand(std::string name)
				{
					auto it = m_commands.find(name);
					m_commands.erase(it);
				}

				void Namespace::RegisterCommand(std::string name, std::string description, std::function<void(Network::TelnetClient*, std::vector<std::string>)> function, std::function<std::string(std::vector<std::string>)> auto_completion)
				{
					Command command = {name, description, function, auto_completion};
					m_commands.insert(std::pair<std::string, Command>(name, command));
				}

				Namespace *Namespace::RegisterNamespace(std::string name, std::string description)
				{
					Namespace *name_space = new Namespace(this, name, description);
					m_namespaces.insert(std::pair<std::string, Namespace*>(name_space->Name(), name_space));

					return name_space;
				}

				void Namespace::RegisterNamespace(Namespace *name_space)
				{
					name_space->SetParent(this);
					m_namespaces.insert(std::pair<std::string, Namespace*>(name_space->Name(), name_space));
				}

				bool Namespace::ExistCommand(std::string name)
				{
					for(const auto& command : m_commands)
						if(command.first == name)
							return true;

					return false;
				}

				bool Namespace::ExistNamespace(std::string name)
				{
					for(const auto& name_space : m_namespaces)
						if(name_space.first == name)
							return true;

					return false;
				}

				std::function<void(Network::TelnetClient*, std::vector<std::string>)> Namespace::GetCommand(std::string name)
				{
					return m_commands[name].function;
				}

				Namespace *Namespace::GetNamespace(std::string name)
				{
					return m_namespaces[name];
				}

				std::map<std::string, std::string> Namespace::List()
				{
					std::map<std::string, std::string> list;
					for(const auto& command : m_commands)
						list.insert(std::pair<std::string, std::string>(command.first, command.second.description));

					for(const auto& name_space : m_namespaces)
						list.insert(std::pair<std::string, std::string>(name_space.first, name_space.second->Description()));

					return list;
				}

				std::string Namespace::AutoComplete(std::string text)
				{
					if(text.empty())
						return std::string();

					std::vector<std::string> args = SystemX::Utils::Split(text, " ");
					if(args.size() > 2 && args[args.size() - 1].empty())
						return std::string();

					std::vector<std::string> commands;
					Command commandSelected;

					for(const auto& command : m_commands)
						if(command.first.compare(0, args[0].length(), args[0]) == 0)
						{
							commands.push_back(command.first);
							commandSelected = command.second;
						}

					for(const auto& name_space : m_namespaces)
						if(name_space.first.compare(0, args[0].length(), args[0]) == 0)
							commands.push_back(name_space.first);

					if(args.size() == 1)
					{
						if(commands.size() == 1)
							return commands[0] + " ";
					}
					else if(commandSelected.auto_completion)
					{
						args.erase(args.begin());
						std::string completeStr = commandSelected.auto_completion(args);
						if(completeStr.empty())
							return completeStr;

						args.insert(args.begin(), commandSelected.name);
						args[args.size() - 1] = completeStr;

						return Utils::Join(args, " ");
					}

					return std::string();
				}

				void Namespace::Help(Network::TelnetClient *client, std::vector<std::string> /*args*/)
				{
					size_t maxCommandLength = 0;
					for(const auto& command : List())
						if(maxCommandLength < command.first.length())
							maxCommandLength = command.first.length();

					for(const auto& command : List())
						client->Send("\r\n" + SystemX::Utils::TableFill(command.first, maxCommandLength) + command.second);
				}

				void Namespace::Exit(Network::TelnetClient *client, std::vector<std::string> /*args*/)
				{
					if(client->CommandInterface()->CurrentNamespace()->Parent())
						client->CommandInterface()->ChangeNamespace(client->CommandInterface()->CurrentNamespace()->Parent());
				}

				Namespace::~Namespace()
				{
					for(const auto& name_space : m_namespaces)
						delete name_space.second;
				}
			}
		}
	}
}
