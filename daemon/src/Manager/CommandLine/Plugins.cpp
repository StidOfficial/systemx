#include "Plugins.hpp"

#include <SystemX/Plugins.hpp>
#include <SystemX/Utils.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				Plugins::Plugins()
					: Namespace(nullptr, "plugins", "Plugins configuration")
				{
					RegisterCommand("show", "Show plugins", std::bind(&Plugins::Show, this, std::placeholders::_1, std::placeholders::_2));
				}

				void Plugins::Show(Network::TelnetClient *client, std::vector<std::string> /*args*/)
				{
					client->Send("\r\nName\t\tVersion");
					client->Send("\r\n---------------------------");

					for(auto &plugin : SystemX::Plugins::All())
						client->Send("\r\n" + SystemX::Utils::TableFill(plugin.second->Name(), 8) + plugin.second->Version());
				}

				Plugins::~Plugins()
				{
				}
			}
		}
	}
}
