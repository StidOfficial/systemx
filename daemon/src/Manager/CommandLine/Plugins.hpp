#ifndef DAEMON_MANAGER_COMMANDLINE_PLUGINS_HPP
#define DAEMON_MANAGER_COMMANDLINE_PLUGINS_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				class Plugins : public Namespace
				{
				public:
					Plugins();
					virtual ~Plugins();
				private:
					void Show(Network::TelnetClient *client, std::vector<std::string> args);
				};
			}
		}
	}
}

#endif
