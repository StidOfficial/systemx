#include "Authenticated.hpp"

#include "Controller.hpp"

#include <json/json.h>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Authenticated::Authenticated()
							: SessionController()
						{
						}

						void Authenticated::Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							Json::Value root;
							root["isAuthenticated"] = IsAuthenticated();

							API::Controller::SendToClient(200, client, response, root);
						}

						Authenticated::~Authenticated()
						{
						}
					}
				}
			}
		}
	}
}
