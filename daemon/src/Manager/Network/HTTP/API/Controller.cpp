#include "Controller.hpp"

#include "Authentication.hpp"
#include "Authenticated.hpp"
#include "Drivers.hpp"
#include "Plugins.hpp"
#include "Equipments.hpp"
#include "Logout.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Controller::Controller()
							: SystemX::Network::HTTP::Controller()
						{
						}

						void Controller::OnMessage(SystemX::Network::HTTP::Client *client, std::vector<char> buffer)
						{
						}

						void Controller::OnRequest(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							if(request->Uri().Path() == "/api/authentication")
								Authentication().OnRequest(client, request, response);
							else if(request->Uri().Path() == "/api/authenticated")
								Authenticated().OnRequest(client, request, response);
							else if(request->Uri().Path() == "/api/drivers")
								Drivers().OnRequest(client, request, response);
							else if(request->Uri().Path() == "/api/plugins")
								Plugins().OnRequest(client, request, response);
							else if(request->Uri().Path() == "/api/equipments")
								Equipments().OnRequest(client, request, response);
							else if(request->Uri().Path() == "/api/logout")
								Logout().OnRequest(client, request, response);
							else
								SendToClient(404, client, response);
						}

						void Controller::SendToClient(int statusCode, SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Response *response, Json::Value root)
						{
							response->SetStatusCode(statusCode);

							Json::StreamWriterBuilder builder;
							std::string output = Json::writeString(builder, root);

							response->Headers()->Add("Content-Length", output.length());
							response->Headers()->Add("Content-Type", "application/json");

							client->Send(response);
							client->Send(output);
						}

						Controller::~Controller()
						{
						}
					}
				}
			}
		}
	}
}
