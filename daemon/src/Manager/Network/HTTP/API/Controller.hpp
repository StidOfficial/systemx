#ifndef DAEMON_MANAGER_NETWORK_HTTP_API_CONTROLLER_HPP
#define DAEMON_MANAGER_NETWORK_HTTP_API_CONTROLLER_HPP

#include <SystemX/Network/HTTP/Controller.hpp>

#include <json/json.h>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						class Controller : public SystemX::Network::HTTP::Controller
						{
						public:
							Controller();
							~Controller();

							void OnMessage(SystemX::Network::HTTP::Client *client, std::vector<char> buffer);
							void OnRequest(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);

							static void SendToClient(int statusCode, SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Response *response, Json::Value root = Json::Value::nullSingleton());
						};
					}
				}
			}
		}
	}
}

#endif
