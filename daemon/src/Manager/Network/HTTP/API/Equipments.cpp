#include "Equipments.hpp"

#include "Controller.hpp"

#include <json/json.h>
#include <SystemX/Daemon/Manager/Equipments.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Equipments::Equipments()
							: SessionController()
						{
						}

						void Equipments::Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							if(IsAuthenticated())
							{
								Json::Value root(Json::arrayValue);

								for(const auto& equipment : Manager::Equipments::All())
								{
									Json::Value obj_equipment;
									obj_equipment["uniqueId"] = equipment.second->UniqueId();
									obj_equipment["name"] = equipment.second->GetName();
									obj_equipment["type"] = static_cast<int>(equipment.second->GetType());
									obj_equipment["description"] = equipment.second->GetDescription();
									obj_equipment["status"] = equipment.second->GetStatus();

									root.append(obj_equipment);
								}

								API::Controller::SendToClient(200, client, response, root);
							}
							else
								API::Controller::SendToClient(403, client, response);
						}

						Equipments::~Equipments()
						{
						}
					}
				}
			}
		}
	}
}
