#ifndef DAEMON_NETWORK_HTTP_LOGOUT_HPP
#define DAEMON_NETWORK_HTTP_LOGOUT_HPP

#include "SessionController.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						class Logout : public SessionController
						{
						public:
							Logout();
							~Logout();

							void Post(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);
						};
					}
				}
			}
		}
	}
}

#endif
