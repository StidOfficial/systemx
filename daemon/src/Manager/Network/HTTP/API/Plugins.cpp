#include "Plugins.hpp"

#include "Controller.hpp"

#include <json/json.h>
#include <SystemX/Plugins.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Plugins::Plugins()
							: SessionController()
						{
						}

						void Plugins::Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							if(IsAuthenticated())
							{
								Json::Value root(Json::arrayValue);

								for(const auto& plugin : SystemX::Plugins::All())
								{
									Json::Value obj_plugin;
									obj_plugin["name"] = plugin.second->Name();
									obj_plugin["version"] = plugin.second->Version();

									root.append(obj_plugin);
								}

								API::Controller::SendToClient(200, client, response, root);
							}
							else
								API::Controller::SendToClient(403, client, response);
						}

						Plugins::~Plugins()
						{
						}
					}
				}
			}
		}
	}
}
