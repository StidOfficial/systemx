#ifndef DAEMON_MANAGER_NETWORK_HTTP_API_PLUGINS_HPP
#define DAEMON_MANAGER_NETWORK_HTTP_API_PLUGINS_HPP

#include "SessionController.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						class Plugins : public SessionController
						{
						public:
							Plugins();
							~Plugins();

							void Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);
						};
					}
				}
			}
		}
	}
}

#endif
