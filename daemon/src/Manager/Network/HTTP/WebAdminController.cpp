#include "WebAdminController.hpp"

#include "API/Controller.hpp"

#include <SystemX/Logger.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					WebAdminController::WebAdminController()
						: SystemX::Network::HTTP::Controller()
					{
					}

					void WebAdminController::OnMessage(SystemX::Network::HTTP::Client *client, std::vector<char> buffer)
					{
						SystemX::Logger::Debug("On message");
					}

					void WebAdminController::OnRequest(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
					{
						if(request->Uri().Path() == "/" && request->Headers()->Get("Upgrade") == "websocket")
							OnRequestWebSocket(client, request, response);
						else if(request->Uri().Path().rfind("/api/", 0) == 0)
							API::Controller().OnRequest(client, request, response);
						else
							OnRequestFile(client, request, response);
					}

					WebAdminController::~WebAdminController()
					{
					}
				}
			}
		}
	}
}
