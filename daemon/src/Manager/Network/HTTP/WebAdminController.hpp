#ifndef DAEMON_MANAGER_NETWORK_HTTP_API_WEBADMINCONTROLLER_HPP
#define DAEMON_MANAGER_NETWORK_HTTP_API_WEBADMINCONTROLLER_HPP

#include <SystemX/Network/HTTP/Controller.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					class WebAdminController : public SystemX::Network::HTTP::Controller
					{
					public:
						WebAdminController();
						~WebAdminController();

						void OnMessage(SystemX::Network::HTTP::Client *client, std::vector<char> buffer);
						void OnRequest(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);
					};
				}
			}
		}
	}
}

#endif
