#ifndef DATABASE_DATABASE_HPP
#define DATABASE_DATABASE_HPP

#include "DatabaseStatement.hpp"

#include <sqlite3.h>
#include <cstdint>
#include <string>

namespace SystemX
{
	namespace Database
	{
		enum class Driver
		{
			SQLite
		};

		class DatabaseStatement;

		class Database
		{
		public:
			Database(Driver driver);
			Driver GetDriver();
			std::string LastError();
			void Open(std::string path);
			DatabaseStatement *Prepare(std::string statement);
			void Query(std::string statement);
			int Exec(std::string statement);
			uint64_t LastInsertRowID();
			void Close();
			~Database();
		private:
			Driver m_driver;
			sqlite3 *m_sqlite;
		};
	}
}

#endif