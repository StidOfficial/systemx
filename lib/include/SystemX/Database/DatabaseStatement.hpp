#ifndef DATABASE_DATABASESTATEMENT_HPP
#define DATABASE_DATABASESTATEMENT_HPP

#include "Database.hpp"

#include <sqlite3.h>
#include <string>
#include <map>

namespace SystemX
{
	namespace Database
	{
		class Database;

		class DatabaseStatement
		{
		public:
			DatabaseStatement();
			DatabaseStatement(Database *database, sqlite3_stmt *statement);
			void Bind(std::string name, int value);
			void Bind(int index, int value);
			void Bind(std::string name, std::string value);
			void Bind(int index, std::string value);
			int GetInt(std::string name);
			std::string GetString(std::string name);
			void Execute();
			bool First();
			bool Next();
			std::string SQL();
			~DatabaseStatement();
		private:
			Database *m_database;
			sqlite3_stmt *m_sqlite_statement;
			std::map<const std::string, int> m_columns;
			std::map<const std::string, int> m_params;

			int GetColumnByName(std::string name);
			int GetParamByName(std::string name);
		};
	}
}

#endif