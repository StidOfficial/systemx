#ifndef DRIVERS_HPP
#define DRIVERS_HPP

#include <SystemX/Driver.hpp>

#include <string>
#include <map>

namespace SystemX
{
	class Drivers
	{
	public:
		static void Initialize();
		static void Register(Driver *driver);
		static std::map<std::string, Driver*> All();
		static Driver *Get(std::string name);
		static void Destroy();
	private:
		static std::map<std::string, Driver*> m_drivers;
	};
}

#endif
