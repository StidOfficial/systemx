#ifndef EQUIPMENT_HARDWARE_HARDWARE_HPP
#define EQUIPMENT_HARDWARE_HARDWARE_HPP

#include <string>

namespace SystemX
{
	namespace Equipment
	{
		namespace Hardware
		{
			class Hardware
			{
			public:
				enum Type
				{
					Motherboard,
					BIOS
				};

				Hardware(Type type);

				virtual Type GetType();
			private:
				Type m_type;
			};
		}
	}
}

#endif
