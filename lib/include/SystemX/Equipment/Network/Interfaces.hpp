#ifndef EQUIPMENT_NETWORK_INTERFACES_HPP
#define EQUIPMENT_NETWORK_INTERFACES_HPP

#include <SystemX/Equipment/Network/Interface.hpp>

#include <vector>

namespace SystemX
{
	namespace Equipment
	{
		class Equipment;

		namespace Network
		{
			class Interfaces
			{
      public:
        Interfaces(Equipment *equipment);
				~Interfaces();

				void Add(Interface *interface);
				Interface *Get(std::string name);
				std::vector<Interface*>::iterator begin();
				std::vector<Interface*>::iterator end();
			private:
				Equipment *m_equipment;
				std::vector<Interface*> m_interfaces;
			};
		}
	}
}

#endif
