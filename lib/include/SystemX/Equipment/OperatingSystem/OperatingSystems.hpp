#ifndef EQUIPMENT_OPERATINGSYSTEM_OPERATINGSYSTEMS_HPP
#define EQUIPMENT_OPERATINGSYSTEM_OPERATINGSYSTEMS_HPP

#include "OperatingSystem.hpp"

#include <vector>

namespace SystemX
{
	namespace Equipment
	{
		namespace OperatingSystem
		{
			class OperatingSystems
			{
			public:
				OperatingSystems();

				OperatingSystem *GetCurrent();
			private:
				std::vector<OperatingSystem*> m_operating_systems;
			};
		}
	}
}

#endif
