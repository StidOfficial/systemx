#ifndef NETWORK_HTTP_HEADER_HPP
#define NETWORK_HTTP_HEADER_HPP

#include <string>
#include <map>
#include <vector>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Headers
			{
			public:
				Headers();

				void Set(std::string key, std::string value);
				void Set(std::string key, std::intmax_t value);
				void Set(std::string key, std::uintmax_t value);
				void Add(std::string key, std::string value);
				void Add(std::string key, std::intmax_t value);
				void Add(std::string key, std::uintmax_t value);
				void Add(std::string key, std::map<std::string, std::string> value);
				void Remove(std::string key);
				std::string Get(std::string key);
				std::vector<std::string> GetAll(std::string key);
				std::size_t Size();
				operator std::multimap<std::string, std::string>()
				{
					return m_headers;
				}
				std::string ToString();
				static Headers* Parse(std::string str);
			private:
				std::multimap<std::string, std::string> m_headers;
			};
		}
	}
}

#endif
