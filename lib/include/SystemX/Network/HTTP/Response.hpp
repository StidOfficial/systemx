#ifndef NETWORK_HTTP_RESPONSE_HPP
#define NETWORK_HTTP_RESPONSE_HPP

#include <SystemX/Network/HTTP/Headers.hpp>
#include <SystemX/Network/HTTP/Cookies.hpp>
#include <SystemX/Network/URI.hpp>

#include <vector>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Client;

			enum class ProtocolVersion
			{
				HTTP1,
				HTTP2
			};

			class Response
			{
			public:
				Response();
				Response(Client *http);
				Response(Client *http, URI uri, Cookies *cookies);
				~Response();

				void SetProtocolVersion(std::string protocolVersion);
				std::string GetProtocolVersion();
				void SetStatusCode(int statusCode);
				int GetStatusCode();
				std::string StatusMessage();

				HTTP::Headers* Headers();
				std::string String();
			private:
				Client *m_client;
				std::string m_protocolVersion;
				int m_statusCode;
				std::string m_statusMessage;
				HTTP::Headers* m_headers;

				int readChunkSize();
			};
		}
	}
}

#endif
