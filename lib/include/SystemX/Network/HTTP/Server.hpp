#ifndef NETWORK_HTTP_SERVER_HPP
#define NETWORK_HTTP_SERVER_HPP

#include "../TCPServer.hpp"
#include "Client.hpp"
#include "Request.hpp"
#include "Response.hpp"
#include "Controller.hpp"

#include "../../Logger.hpp"

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Server : public TCPServer<Client>
			{
			public:
				Server();
				~Server();

				void OnRequest(Client *client, Request *request, Response *response);
				void AddController(std::string host, Controller *controller);
				void RemoveController(std::string host);
			private:
				std::map<std::string, Controller*> m_controllers;
			};
		}
	}
}

#endif
