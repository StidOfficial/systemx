#ifndef NETWORK_SOCKET_HPP
#define NETWORK_SOCKET_HPP

#include "Network.hpp"
#include "InetAddress.hpp"

#include <cstdint>
#include <cstddef>
#include <string>
#include <sys/types.h>
#include <stdexcept>

namespace SystemX
{
	namespace Network
	{
		template <class T>
		class SocketServer;

		class SocketException : public std::runtime_error
		{
		public:
			SocketException(const std::string& what_arg)
				: std::runtime_error(what_arg)
			{
			}
		};

		class Socket
		{
		public:
			Socket(ProtocolFamily domain, SocketType type, int protocol, InetAddress *address);
			Socket(ProtocolFamily domain, SocketType type, int protocol);
			Socket(int socket, InetAddress *address, SocketServer<Socket> *server);
			virtual ~Socket();

			SocketServer<Socket> *Server();
			void SetAddress(InetAddress *address);
			InetAddress *GetAddress();
			void Connect(std::string address, uint16_t port);
			virtual void ReceiveLoop();
			ssize_t Recv(std::vector<unsigned char> data);
			ssize_t Recv(void *buf);
			ssize_t Recv(void *buf, std::size_t len);
			void Send(std::string text);
			void Send(std::vector<char> data);
			void Send(std::vector<char> data, std::size_t len);
			void Send(const void *buf, std::size_t len);
			bool IsConnected();
			void Shutdown();
			void Close();
		private:
			int m_socket;
			SocketServer<Socket> *m_server;
			InetAddress *m_address;
			bool m_shutdown;
		};
	}
}

#endif
