#ifndef NETWORK_TCPCLIENT_HPP
#define NETWORK_TCPCLIENT_HPP

#include "Socket.hpp"

namespace SystemX
{
	namespace Network
	{
		class TCPClient : public Socket
		{
		public:
			TCPClient();
			TCPClient(int socket, InetAddress *address, SocketServer<Socket> *server);
			virtual ~TCPClient();
		};
	}
}

#endif
