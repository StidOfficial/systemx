#ifndef NETWORK_TCPSERVER_HPP
#define NETWORK_TCPSERVER_HPP

#include "SocketServer.hpp"
#include "TCPClient.hpp"

namespace SystemX
{
	namespace Network
	{
		template <class T=TCPClient>
		class TCPServer : private SocketServer<T>
		{
		public:
			TCPServer()
				: SocketServer<T>(ProtocolFamily::IPv4, SocketType::Stream, ProtocolType::TCP)
			{
			}

			virtual void Bind(InetAddress address)
			{
				SocketServer<T>::Bind(address);
			}

			virtual void Listen(int backlog)
			{
				SocketServer<T>::Listen(backlog);
			}

			virtual T* Accept()
			{
				return SocketServer<T>::Accept();
			}

			virtual void Listen()
			{
				SocketServer<T>::Listen();
			}

			virtual std::vector<T*> GetSockets()
			{
				return SocketServer<T>::GetSockets();
			}

			virtual void Shutdown()
			{
				SocketServer<T>::Shutdown();
			}

			virtual void Close()
			{
				SocketServer<T>::Close();
			}
		};
	}
}

#endif
