#ifndef NETWORK_TELNETCLIENT_HPP
#define NETWORK_TELNETCLIENT_HPP

#include "TCPClient.hpp"

#include <vector>

namespace SystemX
{
	namespace Network
	{
		enum class TelnetCommand
		{
			SE = 0xF0,
			NoOperation = 0xF1,
			DataMark = 0xF2,
			Break = 0xF3,
			InterruptProcess = 0xF4,
			AboutOuput = 0xF5,
			AreYouThere = 0xF6,
			EraseCharacter = 0xF7,
			EraseLine = 0xF8,
			GoAhead = 0xF9,
			SB = 0xFA,
			WILL = 0xFB,
			WONT = 0xFC,
			DO = 0xFD,
			DONT = 0xFE,
			IAC = 0xFF
		};

		enum class TelnetOption
		{
			BinaryTransmission = 0x0,
			Echo = 0x1,
			Reconnection = 0x2,
			SuppressGoAhead = 0x3,
			ApproxMessageSizeNegotiation = 0x4,
			Status = 0x5,
			TimingMark = 0x6,
			RemoteControlledTransAndEcho = 0x7,
			OuputLineWidth = 0x8,
			OuputPageSize = 0x9,
			OutputCarriageReturnDisposition = 0xA,
			OutputHorizontalTabStops = 0xB,
			OutputHorizontalTabDisposition = 0xC,
			OutputFormfeedDisposition = 0xD,
			OutputVerticalTabstops = 0xE,
			OutputVerticalTabDisposition = 0xF,
			OutputLinefeedDisposition = 0x10,
			ExtendedASCII = 0x11,
			Logout = 0x12,
			ByteMacro = 0x13,
			DataEntryTerminal = 0x14,
			SUPDUP = 0x15,
			SUPDUPOutput = 0x16,
			SendLocation = 0x17,
			TerminalType = 0x18,
			EndOfRecord = 0x19,
			ACACSUserIdentification = 0x1A,
			OutputMarking = 0x1B,
			TerminalLocationNumber = 0x1C,
			Telnet3270Regime = 0x1D,
			X3PAD = 0x1E,
			NegotiateAboutWindowSize = 0x1F,
			TerminalSpeed = 0x20,
			RemoteFlowControl = 0x21,
			Linemode = 0x22,
			XDisplayLocation = 0x23,
			NewEnvironmentOption = 0x27,
			ExtendedOptionsList = 0xFF,
		};

		class TelnetClient : public TCPClient
		{
		public:
			TelnetClient(int socket, InetAddress *address, SocketServer<Socket> *server);

			void SetOption(TelnetCommand command, TelnetOption option);
			void SendOption();
			void SendCommand(TelnetCommand command);

			void ReadIAC();
		private:
			std::vector<char> m_options_buffer;
		};
	}
}

#endif
