#ifndef NETWORK_URL_HPP
#define NETWORK_URL_HPP

#include <string>
#include <map>

namespace SystemX
{
	namespace Network
	{
		class URI
		{
		public:
			URI();
			URI(std::string url);
			std::string Scheme();
			std::string Host();
			int Port();
			std::string Path();
			std::string AbsolutePath();
			std::map<std::string, std::string> Query();
			std::string QueryToString();
		private:
			std::string m_scheme;
			std::string m_authority;
			std::string m_host;
			int m_port;
			std::string m_path;
			std::map<std::string, std::string> m_query;
			std::string m_fragment;

			void parseQuery(std::string query);
		};
	}
}

#endif
