#ifndef SMBIOS_BIOSINFORMATION_HPP
#define SMBIOS_BIOSINFORMATION_HPP

#include <cstdint>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class BIOSInformation
		{
		public:
			BIOSInformation(std::string vendor, std::string version, uint16_t startingAddressSegment, std::string releaseDate,
					uint8_t ROMSize, uint64_t characteristics, uint8_t characteristicsExtensionByte1,
					uint8_t characteristicsExtensionByte2, uint8_t systemBIOSMajorRelease, uint8_t systemBIOSMinorRelease,
					uint8_t embeddedControllerFirmwareMajorRelease, uint8_t embeddedControllerFirmwareMinorRelease,
					uint16_t extendedBIOSROMSize);
			std::string Vendor();
			std::string Version();
			uint16_t StartingAddressSegment();
			std::string ReleaseDate();
			uint8_t ROMSize();
			bool Characteristics(uint8_t bit);
			bool CharacteristicsExtensionByte1(uint8_t bit);
			bool CharacteristicsExtensionByte2(uint8_t bit);
			uint8_t SystemBIOSMajorRelease();
			uint8_t SystemBIOSMinorRelease();
			uint8_t EmbeddedControllerFirmwareMajorRelease();
			uint8_t EmbeddedControllerFirmwareMinorRelease();
			bool BIOSCharacteristicsNotSupported();
			bool ISASupported();
			bool MCASupported();
			bool EISASupported();
			bool PCISupported();
			bool PCMCIASupported();
			bool PlugAndPlaySupported();
			bool APMSupported();
			bool BIOSUpgradable();
			bool BIOSShadowingAllowed();
			bool VLVESASupported();
			bool ESCDAvailable();
			bool BootFromCDSupported();
			bool SelectableBootSupported();
			bool BIOSROMSocketed();
			bool BootFromPCMCIASupported();
			bool EDDSpecificationSupported();
			bool ACPISupported();
			bool USBLegacySupported();
			bool AGPSupported();
			bool I2OBootSupported();
			bool LS120BootSupported();
			bool ATAPIZIPDriveBootSupported();
			bool SmartBatterySupported();
			bool BIOSBootSpecficiationSupported();
			bool TargetedContentDistribution();
			bool UEFISupported();
		private:
			std::string m_vendor, m_version;
			uint16_t m_startingAddressSegment;
			std::string m_releaseDate;
			uint8_t m_ROMSize;
			uint64_t m_characteristics;
			uint8_t m_characteristicsExtensionByte1, m_characteristicsExtensionByte2;
			uint8_t m_systemBIOSMajorRelease, m_systemBIOSMinorRelease;
			uint8_t m_embeddedControllerFirmwareMajorRelease, m_embeddedControllerFirmwareMinorRelease;
			uint16_t m_extendedBIOSROMSize;
		};
	}
}

#endif
