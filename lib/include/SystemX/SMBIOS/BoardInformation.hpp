#ifndef SMBIOS_BOARDINFORMATION_HPP
#define SMBIOS_BOARDINFORMATION_HPP

#include <cstdint>
#include <string>
#include <vector>

namespace SystemX
{
	namespace SMBIOS
	{
		class BoardInformation
		{
		public:
			enum class BoardType
			{
				Unknown = 0x1,
				Other = 0x2,
				ServerBlade = 0x3,
				ConnectivitySwitch = 0x4,
				SystemManagementModule = 0x5,
				ProcessorModule = 0x6,
				IOModule = 0x7,
				MemoryModule = 0x8,
				DaughterBoard = 0x9,
				Motherboard = 0xA,
				ProcessorMemoryModule = 0xB,
				ProcessorIOModule = 0xC,
				InterconnectBoard = 0xD
			};

			BoardInformation(std::string manufacturer, std::string product, std::string version,
						std::string serialNumber, std::string assetTag, uint8_t featureFlags,
						std::string locationInChassis, uint16_t chassisHandle, BoardType boardType,
						std::vector<uint16_t> containedObjectHandles);
			std::string Manufacturer();
			std::string Product();
			std::string Version();
			std::string SerialNumber();
			std::string AssetTag();
			bool FeatureFlag(uint8_t bit);
			bool IsHotSwappable();
			bool IsReplaceable();
			bool IsRemovable();
			bool RequiresAtLeastOneDaughterBoard();
			bool IsHostingBoard();
			std::string LocationInChassis();
			uint16_t ChassisHandle();
			BoardType GetBoardType();
			std::vector<uint16_t> ContainedObjectHandles();
		private:
			std::string m_manufacturer, m_product, m_version, m_serialNumber, m_assetTag;
			uint16_t m_featureFlags;
			std::string m_locationInChassis;
			uint16_t m_chassisHandle;
			BoardType m_boardType;
			std::vector<uint16_t> m_containedObjectHandles;
		};
	}
}

#endif
