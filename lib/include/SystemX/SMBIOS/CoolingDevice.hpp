#ifndef SMBIOS_COOLINGDEVICE_HPP
#define SMBIOS_COOLINGDEVICE_HPP

#include <cstdint>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class CoolingDevice
		{
		public:
			enum class Type
			{
				Other = 0x1,
				Unknown = 0x2,
				Fan = 0x3,
				CentrifugalBlower = 0x4,
				ChipFan = 0x5,
				CabinetFan = 0x6,
				PowerSupplyFan = 0x7,
				HeatPipe = 0x8,
				IntegratedRefrigeration = 0x9,
				ActiveCooling = 0xA,
				PassiveCooling = 0xB
			};
			enum class Status
			{
				Other = 0x1,
				Unknown = 0x2,
				OK = 0x3,
				NonCritical = 0x4,
				Critical = 0x5,
				NonRecoverable = 0x6
			};
			CoolingDevice(uint16_t temperatureProbeHandle, uint8_t deviceTypeAndStatus, uint8_t coolingUnitGroup, uint32_t OEMDefined, uint16_t nominalSpeed,
					std::string description);
			uint16_t TemperatureProbeHandle();
			Type DeviceType();
			Status DeviceStatus();
			uint8_t CoolingUnitGroup();
			uint32_t OEMDefined();
			uint16_t NominalSpeed();
			std::string Description();
		private:
			uint16_t m_temperatureProbeHandle;
			uint8_t m_deviceTypeAndStatus;
			uint8_t m_coolingUnitGroup;
			uint32_t m_OEMDefined;
			uint16_t m_nominalSpeed;
			std::string m_description;
		};
	}
}

#endif
