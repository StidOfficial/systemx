#ifndef SMBIOS_MANAGEMENTDEVICETHRESHOLDDATA_HPP
#define SMBIOS_MANAGEMENTDEVICETHRESHOLDDATA_HPP

#include <cstdint>

namespace SystemX
{
	namespace SMBIOS
	{
		class ManagementDeviceThresholdData
		{
		public:
			ManagementDeviceThresholdData(uint16_t lowerThresholdNonCritical, uint16_t upperThresholdNonCritical, uint16_t lowerThresholdCritical,
							uint16_t upperThresholdCritical, uint16_t lowerThresholdNonRecoverable, uint16_t upperThresholdNonRecoverable);
			uint16_t LowerThresholdNonCritical();
			uint16_t UpperThresholdNonCritical();
			uint16_t LowerThresholdCritical();
			uint16_t UpperThresholdCritical();
			uint16_t LowerThresholdNonRecoverable();
			uint16_t UpperThresholdNonRecoverable();
		private:
			uint16_t m_lowerThresholdNonCritical, m_upperThresholdNonCritical, m_lowerThresholdCritical, m_upperThresholdCritical;
			uint16_t m_lowerThresholdNonRecoverable, m_upperThresholdNonRecoverable;
		};
	}
}

#endif
