#ifndef SMBIOS_PORTCONNECTORINFORMATION_HPP
#define SMBIOS_PORTCONNECTORINFORMATION_HPP

#include <cstdint>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class PortConnectorInformation
		{
		public:
			enum class ConnectorType
			{
				None,
				Centronics,
				MiniCentronics,
				Proprietary,
				DB25PinMale,
				DB25PinFemale,
				DB15PinMale,
				DB15PinFemale,
				DB9PinMale,
				DB9PinFemale,
				PJ11,
				RJ45,
				MiniSCSI,
				MiniDIN,
				MicroDIN,
				PS2,
				Infrared,
				HPHIL,
				AccessBus,
				SSASCSI,
				CircularDIN8Male,
				CurcularDIN8Female,
				OnBoardIDE,
				OnBoardFloppy,
				DualInline9Pin,
				DualInline25Pin,
				DualInline50Pin,
				DualInline68Pin,
				OnBoardSoundInputFromCDROM,
				MiniCentronicsType14,
				MiniCentronicsType26,
				MiniJack,
				BNC,
				FireWire1394,
				SASSATAPlugReceptacle,
				USBTypeCReceptacle,
				PC98,
				PC98Hireso,
				PCH98,
				PC98Note,
				PC98Full,
				Other
			};

			enum class PortTypes
			{
				None,
				ParallelPortXTATCompatible,
				ParallelPortPS2,
				ParallelPortECP,
				ParallelPortEPP,
				ParallelPortECPEPP,
				SerialPortXTATCompatible,
				SerialPort16450Compatible,
				SerialPort16550Compatible,
				SerialPort16550ACompatible,
				SCSIPort,
				MIDIPort,
				JoyStickPort,
				KeyboardPort,
				MousePort,
				SSASCSI,
				USB,
				FireWireP1394,
				PCMCIATypeI,
				PCMCIATypeII,
				PCMCIATypeIII,
				Cardbus,
				AccessBusPort,
				SCSIII,
				SCSIWide,
				PC98,
				PC98Hireso,
				PCH98,
				VideoPort,
				AudioPort,
				ModemPort,
				NetworkPort,
				SATA,
				SAS,
				MFDP,
				Thunderbolt,
				Serial8251Compatible,
				Serial8251FIFOCompatible,
				Other
			};

			PortConnectorInformation(std::string internalReferenceDesignator, ConnectorType internalConnectorType, std::string externalReferenceDesignator,
							ConnectorType externalConnectorType, PortTypes portType);
			std::string InternalReferenceDesignator();
			ConnectorType InternalConnectorType();
			std::string ExternalReferenceDesignator();
			ConnectorType ExternalConnectorType();
			PortTypes GetPortType();
		private:
			std::string m_internalReferenceDesignator;
			ConnectorType m_internalConnectorType;
			std::string m_externalReferenceDesignator;
			ConnectorType m_externalConnectorType;
			PortTypes m_portType;
		};
	}
}

#endif
