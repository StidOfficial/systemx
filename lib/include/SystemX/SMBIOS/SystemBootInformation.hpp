#ifndef SMBIOS_SYSTEMBOOTINFORMATION_HPP
#define SMBIOS_SYSTEMBOOTINFORMATION_HPP

#include <cstdint>

namespace SystemX
{
	namespace SMBIOS
	{
		class SystemBootInformation
		{
		public:
			SystemBootInformation(uint8_t *reserved, uint8_t *bootStatus);
			uint8_t *Reserved();
			uint8_t *BootStatus();
		private:
			uint8_t *m_reserved;
			uint8_t *m_bootStatus;
		};
	}
}

#endif
