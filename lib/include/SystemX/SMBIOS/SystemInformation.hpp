#ifndef SMBIOS_SYSTEMINFORMATION_HPP
#define SMBIOS_SYSTEMINFORMATION_HPP

#include <cstdint>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class SystemInformation
		{
		public:
			struct UniversalUniqueIdentifier
			{
				uint32_t timeLow;
				uint16_t timeMid;
				uint16_t timeHiAndVersion;
				uint8_t clockSeqHiAndReserved;
				uint8_t clockSeqLow;
				uint8_t node[6];
			};

			enum class WakeUpType
			{
				Reserved,
				Other,
				Unknown,
				APMTimer,
				ModemRing,
				LANRemote,
				PowerSwitch,
				PCIPME,
				ACPowerRestored
			};

			SystemInformation(std::string vendor, std::string productName,  std::string version,
						std::string serialNumber, UniversalUniqueIdentifier UUID,
						WakeUpType wakeUpType, std::string SKUNumber, std::string family);
			std::string Manufacturer();
			std::string ProductName();
			std::string Version();
			std::string SerialNumber();
			UniversalUniqueIdentifier UUID();
			WakeUpType WakeUp();
		private:
			std::string m_manufacturer, m_productName, m_version, m_serialNumber;
			UniversalUniqueIdentifier m_UUID;
			WakeUpType m_wakeUpType;
			std::string m_SKUNumber, m_family;
		};
	}
}

#endif
