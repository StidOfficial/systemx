#ifndef SMBIOS_SYSTEMSLOT_HPP
#define SMBIOS_SYSTEMSLOT_HPP

#include <cstdint>
#include <string>
#include <vector>

namespace SystemX
{
	namespace SMBIOS
	{
		class SystemSlot
		{
		public:
			enum class Type
			{
				Other = 0x1,
				Unknown = 0x2,
				ISA = 0x3,
				MCA = 0x4,
				EISA = 0x5,
				PCI = 0x6,
				PCMCIA = 0x7,
				VLVESA = 0x8,
				Proprietary = 0x9,
				ProcessorCardSlot = 0xA,
				ProprietaryMemoryCardSlot = 0xB,
				IORiserCardSlot = 0xC,
				NuBus = 0xD,
				PCI66MHzCapable = 0xE,
				AGP = 0xF,
				AGP2X = 0x10,
				AGP4X = 0x11,
				PCIX = 0x12,
				AGP8X = 0x13,
				M2Socket1DP = 0x14,
				M2Socket1SD = 0x15,
				M2Socket2 = 0x16,
				M2Socket3 = 0x17,
				MXMTypeI = 0x18,
				MXMTypeII = 0x19,
				MXMTypeIIIStandardConnector = 0x1A,
				MXMTypeIIIHEConnector = 0x1B,
				MXMTypeIV = 0x1C,
				MXM3TypeA = 0x1D,
				MXM3TypeB = 0x1E,
				PCIExpressGen2SFF8639 = 0x1F,
				PCIExpressGen3SFF8639 = 0x20,
				PCIExpressMini = 0x21,
				//PCIExpressMini = 0x22,
				PCExpressMini76Pin = 0x23,
				PC98C20 = 0xA0,
				PC98C24 = 0xA1,
				PC98E = 0xA2,
				PC98LocalBus = 0xA3,
				PC98Card = 0xA4,
				PCIExpress = 0xA5,
				PCIExpressx1 = 0xA6,
				PCIExpressx2 = 0xA7,
				PCIExpressx4 = 0xA8,
				PCIExpressx8 = 0xA9,
				PCIExpressx16 = 0xAA,
				PCIExpressGen2 = 0xAB,
				PCIExpressGen2x1 = 0xAC,
				PCIExpressGen2x2 = 0xAD,
				PCIExpressGen2x4 = 0xAE,
				PCIExpressGen2x8 = 0xAF,
				PCIExpressGen2x16 = 0xB0,
				PCIExpressGen3 = 0xB1,
				PCIExpressGen3x1 = 0xB2,
				PCIExpressGen3x2 = 0xB3,
				PCIExpressGen3x4 = 0xB4,
				PCIExpressGen3x8 = 0xB5,
				PCIExpressGen3x16 = 0xB6,
			};

			enum class SlotDataBusWidth
			{
				Other = 0x1,
				Unknown = 0x2,
				Bit8 = 0x3,
				Bit16 = 0x4,
				Bit32 = 0x5,
				Bit64 = 0x6,
				Bit128 = 0x7,
				x1 = 0x8,
				x2 = 0x9,
				x4 = 0xA,
				x8 = 0xB,
				x12 = 0xC,
				x16 = 0xD,
				x32 = 0xE
			};

			enum class CurrentUsage
			{
				Other = 0x1,
				Unknown = 0x2,
				Available = 0x3,
				InUse = 0x4,
				Unavailable = 0x5
			};

			enum class Length
			{
				Other = 0x1,
				Unknown = 0x2,
				ShortLength = 0x3,
				LongLength = 0x4,
				Factor2_5 = 0x5,
				Factor3_5 = 0x6
			};

			SystemSlot(std::string designation, Type type, SlotDataBusWidth slotDataBusWidth, CurrentUsage currentUsage, Length length, uint16_t id,
					uint8_t characteristics1, uint8_t characteristics2, uint16_t segmentGroupNumber, uint8_t busNumber, uint8_t deviceFunctionNumber,
					uint8_t dataBusWidth, std::vector<uint8_t> peerGroups);
			std::string Designation();
			Type GetType();
			SlotDataBusWidth GetSlotDataBusWidth();
			CurrentUsage GetCurrentUsage();
			Length GetLength();
			uint16_t ID();
			uint16_t SegmentGroupNumber();
			uint8_t BusNumber();
			uint8_t DeviceFunctionNumber();
			uint8_t DataBusWidth();
			std::vector<uint8_t> PeerGroups();
		private:
			std::string m_designation;
			Type m_type;
			SlotDataBusWidth m_slotDataBusWidth;
			CurrentUsage m_currentUsage;
			Length m_length;
			uint16_t m_id;
			uint8_t m_characteristics1, m_characteristics2;
			uint16_t m_segmentGroupNumber;
			uint8_t m_busNumber, m_deviceFunctionNumber, m_dataBusWidth;
			std::vector<uint8_t> m_peerGroups;
		};
	}
}

#endif
