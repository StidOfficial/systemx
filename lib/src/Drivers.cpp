#include <SystemX/Drivers.hpp>

namespace SystemX
{
	std::map<std::string, Driver*> Drivers::m_drivers;

	void Drivers::Initialize()
	{
		for(const auto& driver : m_drivers)
			driver.second->Initialize();
	}

	void Drivers::Register(Driver *driver)
	{
		m_drivers.insert(std::pair<std::string, Driver*>(driver->Name(), driver));
	}

	std::map<std::string, Driver*> Drivers::All()
	{
		return m_drivers;
	}

	Driver *Drivers::Get(std::string name)
	{
		for(const auto& driver : m_drivers)
			if(driver.second->Name() == name)
				return driver.second;

		return nullptr;
	}

	void Drivers::Destroy()
	{
		for(const auto& driver : m_drivers)
			driver.second->Uninitialize();
	}
}
