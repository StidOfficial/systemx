#include <SystemX/Equipment/Hardware/Hardwares.hpp>

namespace SystemX
{
	namespace Equipment
	{
		namespace Hardware
		{
			Hardwares::Hardwares(std::vector<CPU> cpus)
				: m_cpus(cpus)
			{
			}


			int Hardwares::CPUInstalled()
			{
				return 1;
			}

			int Hardwares::CPUAvalaible()
			{
				return 1;
			}
		}
	}
}
