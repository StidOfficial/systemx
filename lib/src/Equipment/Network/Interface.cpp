#include <SystemX/Equipment/Network/Interface.hpp>

namespace SystemX
{
	namespace Equipment
	{
		namespace Network
		{
			Interface::Interface(std::string name)
				: m_name(name)
			{

			}

			void Interface::SetState(bool state)
			{
				m_state = state;
			}

			bool Interface::GetState()
			{
				return m_state;
			}

			void Interface::SetName(std::string name)
			{
				m_name = name;
			}

			std::string Interface::GetName()
			{
				return m_name;
			}

			void Interface::SetMACAddress(std::string macAddress)
			{
				m_macAddress = macAddress;
			}

			std::string Interface::GetMACAddress()
			{
				return m_macAddress;
			}
		}
	}
}
