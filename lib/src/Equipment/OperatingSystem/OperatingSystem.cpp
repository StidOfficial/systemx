#include <SystemX/Equipment/OperatingSystem/OperatingSystem.hpp>

namespace SystemX
{
	namespace Equipment
	{
		namespace OperatingSystem
		{
			OperatingSystem::OperatingSystem()
			{
			}

			void OperatingSystem::SetHostname(std::string hostname)
			{
				m_hostname = hostname;
			}

			std::string OperatingSystem::GetHostname()
			{
				return m_hostname;
			}

			Software::Softwares *OperatingSystem::Softwares()
			{
				return m_softwares;
			}

			OperatingSystem::~OperatingSystem()
			{
			}
		}
	}
}
