#include <SystemX/Logger.hpp>

#include <cstdio>
#include <vector>
#include <ctime>

namespace SystemX
{
	void Logger::Trace(std::string format, ...)
	{
		va_list args;
		va_start (args, format);

		Print(LogLevel::TRACE, format, args);

		va_end (args);
	}

	void Logger::Debug(std::string format, ...)
	{
		va_list args;
		va_start(args, format);

		Print(LogLevel::DEBUG, format, args);

		va_end(args);
	}

	void Logger::Info(std::string format, ...)
	{
		va_list args;
		va_start(args, format);

		Print(LogLevel::INFO, format, args);

		va_end(args);
	}

	void Logger::Warn(std::string format, ...)
	{
		va_list args;
		va_start(args, format);

		Print(LogLevel::WARN, format, args);

		va_end (args);
	}

	void Logger::Error(std::string format, ...)
	{
		va_list args;
		va_start(args, format);

		Print(LogLevel::ERROR, format, args);

		va_end(args);
	}

	void Logger::Print(LogLevel level, std::string format, va_list args)
	{
		std::string time;
		time.resize(20);

		std::time_t now = std::time(nullptr);
		int len = std::strftime(&time[0], time.size(), "%Y-%m-%d %X", std::localtime(&now));
		time.resize(len);

		std::string final_format = "[" + time + "][" + GetLevel(level) + "] " + format + "\n";
		std::vprintf(final_format.c_str(), args);
	}

	std::string Logger::GetLevel(LogLevel level)
	{
		switch(level)
		{
			case LogLevel::TRACE:
				return "TRACE";
			case LogLevel::DEBUG:
				return "DEBUG";
			case LogLevel::INFO:
				return "INFO";
			case LogLevel::WARN:
				return "WARN";
			case LogLevel::ERROR:
				return "ERROR";
			default:
				return "UNKNOWN";
		}
	}
}
