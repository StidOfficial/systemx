#include <SystemX/MimeTypes.hpp>

namespace SystemX
{
	const std::map<std::string, std::string> MimeTypes::m_mimetypes = {
		{"html", "text/html"},
		{"css", "text/css"},
		{"js", "application/javascript"},
		{"ico", "image/x-icon"},
		{"woff2", "font/woff2"}
	};

	std::string MimeTypes::find(std::experimental::filesystem::path path)
	{
		std::string extension = path.extension().string();
		if(extension.empty() || extension.length() <= 1)
			return std::string();

		auto it = MimeTypes::m_mimetypes.find(extension.substr(1));
		if(it != MimeTypes::m_mimetypes.end())
			return it->second;

		return std::string();
	}
}
