#include <SystemX/Network/HTTP/Client.hpp>

#include <SystemX/Network/HTTP/Response.hpp>
#include <SystemX/Network/HTTP/Request.hpp>
#include <SystemX/Network/HTTP/Server.hpp>

#include <SystemX/Logger.hpp> // Tmp

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Client::Client()
				: SystemX::Network::TCPClient()
			{
			}

			Client::Client(int socket, SystemX::Network::InetAddress *address, SystemX::Network::SocketServer<SystemX::Network::Socket> *server)
				: SystemX::Network::TCPClient(socket, address, server)
			{
			}

			void Client::ReceiveLoop()
			{
				Request *request = nullptr;
				Response *response = nullptr;

				try
				{
					request = new Request(this);
					response = new Response();

					response->SetProtocolVersion("HTTP/1.1");
					response->SetStatusCode(400);

					HTTP::Server *httpServer = reinterpret_cast<HTTP::Server*>(Server());
					httpServer->OnRequest(this, request, response);
				}
				catch(SystemX::Network::SocketException& e)
				{
				}

				if(request)
					delete request;

				if(response)
					delete response;
			}

			Response *Client::Send(Request request, Cookies *cookies, std::vector<char> data)
			{
				Connect(request.Uri().Host(), request.Uri().Port());

				Socket::Send(request.ToString());
				if(data.size() > 0)
					Socket::Send(data);

				return new Response(this, request.Uri(), cookies);
			}


			void Client::Send(std::string text)
			{
				Socket::Send(text);
			}

			void Client::Send(std::vector<char> data)
			{
				Socket::Send(data);
			}

			void Client::Send(std::vector<char> data, std::size_t len)
			{
				Socket::Send(data, len);
			}

			void Client::Send(const void *buf, std::size_t len)
			{
				Socket::Send(buf, len);
			}

			void Client::Send(Response *response)
			{
				Send(response, std::vector<char>());
			}

			void Client::Send(Response *response, std::vector<char> data)
			{
				Socket::Send(response->String());
				if(data.size() > 0)
					Socket::Send(data);
			}

			Response *Client::Get(URI uri, Headers headers, Cookies *cookies)
			{
				return Send(Request(Method::GET, uri, headers, cookies), cookies);
			}

			Response *Client::Post(URI uri, Headers headers, Cookies *cookies, std::vector<char> data)
			{
				headers.Set("Content-Length", std::to_string(data.size()));

				return Send(Request(Method::POST, uri, headers, cookies), cookies, data);
			}

			Client::~Client()
			{
			}
		}
	}
}
