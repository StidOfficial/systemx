#include <SystemX/Network/HTTP/Cookie.hpp>

#include <SystemX/Network/HTTP/HTTP.hpp>
#include <SystemX/Utils.hpp>

#include <vector>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Cookie::Cookie(std::string name, std::string value, std::string domain)
				: Cookie(name, value, -1, "/", domain, true, false)
			{
			}

			Cookie::Cookie(std::string name, std::string value, std::string expires, std::string path, std::string domain, bool secure, bool httpOnly)
				: Cookie(name, value, StringToTime(expires), path, domain, secure, httpOnly)
			{
			}

			Cookie::Cookie(std::string name, std::string value, time_t expires, std::string path, std::string domain, bool secure, bool httpOnly)
				: m_name(name), m_value(value), m_expires(expires), m_path(path), m_domain(domain), m_secure(secure), m_httpOnly(httpOnly)
			{
			}

			std::string Cookie::Name()
			{
				return m_name;
			}

			std::string Cookie::Value()
			{
				return m_value;
			}

			time_t Cookie::Expires()
			{
				return m_expires;
			}

			std::string Cookie::Path()
			{
				return m_path;
			}

			std::string Cookie::Domain()
			{
				return m_domain;
			}

			bool Cookie::IsSecure()
			{
				return m_secure;
			}

			bool Cookie::IsHTTPOnly()
			{
				return m_httpOnly;
			}

			Cookie* Cookie::Parse(std::string str, std::string defaultDomain)
			{
				std::vector<std::string> items = Utils::Split(str, ";");
				if(items.size() == 0)
					return nullptr;

				std::string name;
				std::string value;
				std::string expires;
				std::string path;
				std::string domain = defaultDomain;

				for(auto it = items.begin(); it < items.end(); it++)
				{
					std::string item = Utils::Trim(*it);
					std::vector<std::string> keyValue = Utils::Split(item, "=", 2);
					if(it == items.begin())
					{
						name = keyValue[0];
						value = keyValue[1];
					}
					else if(keyValue[0] == "expires")
					{
					}
					else if(keyValue[0] == "path")
						path = keyValue[1];
				}

				return new Cookie(name, value, expires, path, domain, false, false);
			}
		}
	}
}
