#include <SystemX/Network/HTTP/HTTP.hpp>

#include <sstream>
#include <iomanip>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			time_t StringToTime(std::string datetime)
			{
				std::stringstream ssDatetime(datetime);
				std::tm tmDatetime;
				ssDatetime >> std::get_time(&tmDatetime, HTTP_DATE_FORMAT);

				std::time_t timeDatetime = -1;
				if(!ssDatetime.fail())
					timeDatetime = timegm(&tmDatetime);

				return timeDatetime;
			}

			std::string TimeToString(time_t datetime)
			{
				std::stringstream ssDatetime;
				ssDatetime << std::put_time(std::gmtime(&datetime), HTTP_DATE_FORMAT);
				return ssDatetime.str();
			}
		}
	}
}
