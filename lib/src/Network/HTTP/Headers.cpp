#include <SystemX/Network/HTTP/Headers.hpp>

#include <SystemX/Utils.hpp>

#include <sstream>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Headers::Headers()
			{
			}

			void Headers::Set(std::string key, std::string value)
			{
				auto it = m_headers.find(key);
				if(it != m_headers.end())
					it->second = value;
				else
					Add(key, value);
			}

			void Headers::Set(std::string key, std::intmax_t value)
			{
				std::ostringstream sstream;
				sstream << value;

				Set(key, sstream.str());
			}

			void Headers::Set(std::string key, std::uintmax_t value)
			{
				std::ostringstream sstream;
				sstream << value;

				Set(key, sstream.str());
			}

			void Headers::Add(std::string key, std::string value)
			{
				m_headers.emplace(key, value);
			}

			void Headers::Add(std::string key, std::intmax_t value)
			{
				std::ostringstream sstream;
				sstream << value;

				Add(key, sstream.str());
			}

			void Headers::Add(std::string key, std::uintmax_t value)
			{
				std::ostringstream sstream;
				sstream << value;

				Add(key, sstream.str());
			}

			void Headers::Add(std::string key, std::map<std::string, std::string> value)
			{
				std::string outputValue;

				for(auto it = value.begin(); it != value.end(); it++)
				{
					outputValue.append(it->first + "=" + it->second);
					if(std::next(it) == value.end())
						outputValue.append("; ");
				}

				Add(key, outputValue);
			}

			void Headers::Remove(std::string key)
			{
				m_headers.erase(key);
			}

			std::string Headers::Get(std::string key)
			{
				auto it = m_headers.find(key);
				if(it != m_headers.end())
					return it->second;

				return std::string();
			}

			std::vector<std::string> Headers::GetAll(std::string key)
			{
				std::vector<std::string> list;
				auto range = m_headers.equal_range(key);
				for(auto it = range.first; it != range.second; it++)
					list.push_back(it->second);

				return list;
			}

			std::size_t Headers::Size()
			{
				return m_headers.size();
			}

			std::string Headers::ToString()
			{
				std::string headers;
				for(const auto& header : m_headers)
					headers.append(header.first + ": " + header.second + "\r\n");

				return headers;
			}

			Headers* Headers::Parse(std::string str)
			{
				Headers *headers = new Headers();

				std::size_t last_pos = 0, pos = 0;
				do
				{
					pos = str.find("\r\n", last_pos);

					std::string keyValue;
					if(pos != std::string::npos)
						keyValue = std::string(str.begin() + last_pos, str.begin() + pos);
					else
						keyValue = std::string(str.begin() + last_pos, str.end());

					std::size_t separatorPos = keyValue.find(":");
					std::string key = keyValue.substr(0, separatorPos);
					std::string value = Utils::Trim(keyValue.substr(separatorPos + 1));

					headers->Add(key, value);

					last_pos = pos + 2;
				}
				while(pos != std::string::npos);

				return headers;
			}
		}
	}
}
