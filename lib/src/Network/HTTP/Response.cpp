#include <SystemX/Network/HTTP/Response.hpp>

#include <SystemX/Network/HTTP/Client.hpp>
#include <SystemX/Network/HTTP/HTTP.hpp>
#include <SystemX/Utils.hpp>

#include <algorithm>
#include <chrono>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Response::Response()
				: m_client(nullptr)
			{
				m_headers = new HTTP::Headers();
				m_headers->Set("Server", "SystemX");

				auto chronoDate = std::chrono::system_clock::now();
				std::time_t timeDate = decltype(chronoDate)::clock::to_time_t(chronoDate);

				m_headers->Set("Date", TimeToString(timeDate));
			}

			Response::Response(Client *client)
				: Response(client, URI(), nullptr)
			{
			}

			Response::Response(Client *client, URI uri, Cookies *cookies)
				: m_client(client)
			{
				const std::string httpNewLine = "\r\n";
				const std::string httpHeaderEnd = "\r\n\r\n";

				std::vector<char> headerBuffer(8000);
				bool is_valid_header = false;
				for(size_t i = 0; i < headerBuffer.size(); i++)
				{
					m_client->Recv(&headerBuffer[i], sizeof(char));

					if(i >= httpHeaderEnd.length())
						if(std::string(headerBuffer.begin() + ((i + 1) - httpHeaderEnd.length()), headerBuffer.begin() + (i + 1)) == httpHeaderEnd)
						{
							is_valid_header = true;
							headerBuffer.resize(i + 1);
							break;
						}
				}

				if(!is_valid_header)
					throw std::runtime_error("Out of range header with 8k bytes limit");

				std::string header(headerBuffer.begin(), headerBuffer.end());
				auto pos = header.find(httpNewLine);
				std::string httpStatus(header.begin(), header.begin() + pos);

				std::vector<std::string> statusParams = SystemX::Utils::Split(httpStatus, " ", 2);
				m_protocolVersion = statusParams[0];
				m_statusCode = std::stoi(statusParams[1]);
				m_statusMessage = statusParams[2];

				header.erase(header.begin(), header.begin() + pos + httpNewLine.length());
				header.erase(header.end() - httpHeaderEnd.length(), header.end());

				m_headers = HTTP::Headers::Parse(header);

				if(cookies)
				{
					std::vector<std::string> setCookies = m_headers->GetAll("Set-Cookie");
					for(auto &cookie : setCookies)
						cookies->Set(Cookie::Parse(cookie, uri.Host()));
				}
			}

			void Response::SetProtocolVersion(std::string protocolVersion)
			{
				m_protocolVersion = protocolVersion;
			}

			std::string Response::GetProtocolVersion()
			{
				return m_protocolVersion;
			}

			void Response::SetStatusCode(int statusCode)
			{
				m_statusCode = statusCode;

				switch(m_statusCode)
				{
					case 100:
						m_statusMessage = "Continue";
						break;
					case 101:
						m_statusMessage = "Switching Protocol";
						break;
					case 102:
						m_statusMessage = "Processing";
						break;
					case 200:
						m_statusMessage = "OK";
						break;
					case 201:
						m_statusMessage = "Created";
						break;
					case 202:
						m_statusMessage = "Accepted";
						break;
					case 203:
						m_statusMessage = "Non-Authoritative Information";
						break;
					case 204:
						m_statusMessage = "No Content";
						break;
					case 205:
						m_statusMessage = "Reset Content";
						break;
					case 206:
						m_statusMessage = "Partial Content";
						break;
					case 207:
						m_statusMessage = "Multi-Status";
						break;
					case 208:
						m_statusMessage = "Multi-Status";
						break;
					case 226:
						m_statusMessage = "IM Used";
						break;
					case 300:
						m_statusMessage = "Multiple Choice";
						break;
					case 301:
						m_statusMessage = "Moved Permanently";
						break;
					case 302:
						m_statusMessage = "Found";
						break;
					case 303:
						m_statusMessage = "See Other";
						break;
					case 304:
						m_statusMessage = "Not Modified";
						break;
					case 305:
						m_statusMessage = "Use Proxy";
						break;
					case 306:
						m_statusMessage = "unused";
						break;
					case 400:
						m_statusMessage = "Bad Request";
						break;
					case 401:
						m_statusMessage = "Unauthorired";
						break;
					case 402:
						m_statusMessage = "Payment Required";
						break;
					case 403:
						m_statusMessage = "Forbidden";
						break;
					case 404:
						m_statusMessage = "Not Found";
						break;
					case 405:
						m_statusMessage = "Method Not Allowed";
						break;
					case 406:
						m_statusMessage = "Not Acceptable";
						break;
					case 407:
						m_statusMessage = "Proxy Authentication Required";
						break;
					case 408:
						m_statusMessage = "Request Timeout";
						break;
					case 409:
						m_statusMessage = "Conflict";
						break;
					case 410:
						m_statusMessage = "Gone";
						break;
					case 411:
						m_statusMessage = "Length Required";
						break;
					case 412:
						m_statusMessage = "Precondition Failed";
						break;
					case 413:
						m_statusMessage = "Payload Too Large";
						break;
					case 414:
						m_statusMessage = "URI Too Long";
						break;
					case 415:
						m_statusMessage = "Unsupported Media Type";
						break;
					case 416:
						m_statusMessage = "Requested Range Not Satisfiable";
						break;
					case 417:
						m_statusMessage = "Expectation Failed";
						break;
					case 418:
						m_statusMessage = "I'm a teapot";
						break;
					case 421:
						m_statusMessage = "Misdirected Request";
						break;
					case 422:
						m_statusMessage = "Unprocessable Entity";
						break;
					case 423:
						m_statusMessage = "Locked";
						break;
					case 424:
						m_statusMessage = "Failed Dependency";
						break;
					case 425:
						m_statusMessage = "Too Early";
						break;
					case 426:
						m_statusMessage = "Upgrade Required";
						break;
					case 428:
						m_statusMessage = "Precondition Required";
						break;
					case 429:
						m_statusMessage = "Too Many Requests";
						break;
					case 431:
						m_statusMessage = "Request Header Fields Too Large";
						break;
					case 451:
						m_statusMessage = "Unavailable For Legal Reasons";
						break;
					case 500:
						m_statusMessage = "Internet Server Error";
						break;
					case 501:
						m_statusMessage = "Not Implemented";
						break;
					case 502:
						m_statusMessage = "Bad Gateway";
						break;
					case 503:
						m_statusMessage = "Service Unavaible";
						break;
					case 504:
						m_statusMessage = "Gateway Timeout";
						break;
					case 505:
						m_statusMessage = "HTTP Version Not Supported";
						break;
					case 506:
						m_statusMessage = "Variant Also Negotiates";
						break;
					case 507:
						m_statusMessage = "Insufficient Storage";
						break;
					case 508:
						m_statusMessage = "Loop Detected";
						break;
					case 510:
						m_statusMessage = "Not Extended";
						break;
					case 511:
						m_statusMessage = "Network Authentification Required";
						break;
				}
			}

			int Response::GetStatusCode()
			{
				return m_statusCode;
			}

			std::string Response::StatusMessage()
			{
				return m_statusMessage;
			}

			HTTP::Headers* Response::Headers()
			{
				return m_headers;
			}

			int Response::readChunkSize()
			{
				const std::string httpNewLine = "\r\n";

				std::vector<char> chunkSizeBuffer(4 + httpNewLine.length());

				bool is_valid_chunk_size = false;
				for(size_t i = 0; i < chunkSizeBuffer.size(); i++)
				{
					m_client->Recv(&chunkSizeBuffer[i], sizeof(char));
					if(i >= httpNewLine.length())
						if(std::string(chunkSizeBuffer.begin() + ((i + 1) - httpNewLine.length()), chunkSizeBuffer.begin() + (i + 1)) == httpNewLine)
						{
							is_valid_chunk_size = true;
							chunkSizeBuffer.resize(i + 1);
							break;
						}
				}

				if(!is_valid_chunk_size)
					throw std::runtime_error("Out of range chunk size with 6 bytes limit");

				std::string chunkSize(chunkSizeBuffer.begin(), chunkSizeBuffer.begin() + (chunkSizeBuffer.size() - httpNewLine.length()));

				return std::stoul(chunkSize, nullptr, 16);
			}

			std::string Response::String()
			{
				std::string data;

				if(m_client)
				{
					bool is_chunked = Headers()->Get("Transfer-Encoding") == "chunked";
					if(!is_chunked)
						is_chunked = Headers()->Get("TE") == "chunked";

					int content_length = -1;
					if(!Headers()->Get("Content-Length").empty())
						content_length = std::stoi(Headers()->Get("Content-Length"));

					if(is_chunked)
					{
						std::vector<char> chunkBuffer;
						std::vector<char> chunkBoundaryBuffer(2);
						while(true)
						{
							int chunkSize = readChunkSize();
							if(chunkSize == 0)
								break;

							chunkBuffer.resize(chunkSize);

							int chunkRecvSize = 0, leftChunkSize = chunkBuffer.size();
							do
							{
								chunkRecvSize = m_client->Recv(&chunkBuffer[chunkRecvSize], leftChunkSize);
								leftChunkSize = leftChunkSize - chunkRecvSize;
							}
							while(leftChunkSize > 0);

							m_client->Recv(&chunkBoundaryBuffer[0], chunkBoundaryBuffer.size());

							data.insert(data.end(), chunkBuffer.begin(), chunkBuffer.end());
						}

						return data;
					}

					std::vector<char> dataBuffer(2048);

					int size, recvLength = 0;
					while(content_length > 0 && recvLength < content_length)
					{
						size = m_client->Recv(&dataBuffer[0], dataBuffer.size());
						data.insert(data.end(), dataBuffer.begin(), dataBuffer.begin() + size);

						recvLength += size;
					}
				}

				data += GetProtocolVersion() + " " + std::to_string(GetStatusCode()) + " " + StatusMessage() + "\r\n";
				data += Headers()->ToString() + "\r\n";

				return data;
			}

			Response::~Response()
			{
				delete m_headers;
			}
		}
	}
}
