#include <SystemX/Network/HTTP/Server.hpp>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Server::Server()
				: TCPServer<Client>()
			{
			}

			void Server::OnRequest(Client *client, Request *request, Response *response)
			{
				try
				{
					if(request->Headers()->Get("Host").empty())
					{
						client->Send(response);
						client->Send(response->StatusMessage());
					}

					auto it = m_controllers.find(request->Headers()->Get("Host"));
					if(it != m_controllers.end())
						it->second->OnRequest(client, request, response);
					else
					{
						client->Send(response);
						client->Send(response->StatusMessage());
					}
				}
				catch(SystemX::Network::SocketException& e)
				{
				}
			}

			void Server::AddController(std::string host, Controller *controller)
			{
				m_controllers.emplace(host, controller);
			}

			void Server::RemoveController(std::string host)
			{
				m_controllers.erase(host);
			}

			Server::~Server()
			{
				for(const auto& it : m_controllers)
					if(it.second)
						delete it.second;
			}
		}
	}
}
