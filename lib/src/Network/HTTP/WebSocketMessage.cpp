#include <SystemX/Network/HTTP/WebSocketMessage.hpp>

#include <SystemX/Logger.hpp> // temp

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			WebSocketMessage::WebSocketMessage(Client *client)
			{
				unsigned char byte[2];
				client->Recv(byte, 2);

				/*bool isFin = ((byte[0] & 0x80) != 0);
				bool rsv1 = ((byte[0] & 0x40) != 0);
				bool rsv2 = ((byte[0] & 0x20) != 0);
				bool rsv3 = ((byte[0] & 0x10) != 0);*/
				m_opCode = static_cast<OpCode>(byte[0] & 0x0F);

				m_isMasked = ((byte[1] & 0x80) != 0);
				int16_t payloadLength = (byte[1] & 0x7F);

				int extendedPayloadByteLength = 0;
				if(payloadLength == 0x7E)
					extendedPayloadByteLength = 2;
				else if(payloadLength == 0x7F)
					extendedPayloadByteLength = 8;

				if(extendedPayloadByteLength > 0)
				{
					payloadLength = 0;
					while(--extendedPayloadByteLength >= 0)
					{
						client->Recv(byte, 1);
						payloadLength |= (byte[0] & 0xFF) << (8 * extendedPayloadByteLength);
					}
				}

				if(m_isMasked)
					client->Recv(m_maskingKey, sizeof(m_maskingKey));

				m_payload.resize(payloadLength);

				unsigned char payload[payloadLength];
				client->Recv(payload, payloadLength);

				if(m_isMasked)
					for(std::size_t i = 0; i < m_payload.size(); i++)
						payload[i] ^= m_maskingKey[i % 4];

				m_payload.assign(payload, payload + payloadLength);
			}

			WebSocketMessage::OpCode WebSocketMessage::GetOpCode()
			{
				return m_opCode;
			}

			std::vector<unsigned char> WebSocketMessage::GetPayload()
			{
				return m_payload;
			}
		}
	}
}
