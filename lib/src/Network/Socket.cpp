#include <SystemX/Network/Socket.hpp>

#include <SystemX/Utils.hpp>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <cassert>
#include <cerrno>
#include <vector>

namespace SystemX
{
	namespace Network
	{
		Socket::Socket(ProtocolFamily domain, SocketType type, int protocol, InetAddress *address)
			: m_server(nullptr), m_address(address), m_shutdown(false)
		{
			m_socket = socket((int)domain, (int)type, protocol);
			if(m_socket < 0)
				throw SocketException("socket() failed: " + Utils::LastError());
		}

		Socket::Socket(ProtocolFamily domain, SocketType type, int protocol)
			: Socket(domain, type, protocol, nullptr)
		{
			m_shutdown = true;
		}

		Socket::Socket(int socket, InetAddress *address, SocketServer<Socket> *server)
			: m_socket(socket), m_server(server), m_address(address), m_shutdown(false)
		{
		}

		SocketServer<Socket> *Socket::Server()
		{
			return m_server;
		}

		void Socket::SetAddress(InetAddress *address)
		{
			delete m_address;
			m_address = address;
		}

		InetAddress *Socket::GetAddress()
		{
			return m_address;
		}

		void Socket::Connect(std::string address, uint16_t port)
		{
			std::vector<InetAddress> inet_address = InetAddress::GetAddressInfo(address);

			int result;
			for(auto &address : inet_address)
			{
				address.SetPort(port);

				result = connect(m_socket, address.GetSocketAddress(), address.GetSocketAddressLength());
				if(result < 0)
					continue;

				m_address = &address;
				m_shutdown = false;
				return;
			}

			throw SocketException(Utils::LastError().c_str());
		}

		void Socket::ReceiveLoop()
		{
		}

		ssize_t Socket::Recv(std::vector<unsigned char> data)
		{
			return Recv(&data[0], data.size());
		}

		ssize_t Socket::Recv(void *buf)
		{
			return Recv(buf, sizeof(buf));
		}

		ssize_t Socket::Recv(void *buf, std::size_t len)
		{
			ssize_t size = recv(m_socket, buf, len, MSG_NOSIGNAL);
			if(size < 0)
				throw SocketException("recv() failed: " + Utils::LastError());
			else if(size == 0 && len > 0)
			{
				m_shutdown = true;
				throw SocketException("Connection closed !");
			}

			return size;
		}

		void Socket::Send(std::string text)
		{
			Send(text.c_str(), text.length());
		}

		void Socket::Send(std::vector<char> data)
		{
			Send(&data[0], data.size());
		}

		void Socket::Send(std::vector<char> data, std::size_t len)
		{
			Send(&data[0], len);
		}

		void Socket::Send(const void *buf, std::size_t len)
		{
			ssize_t result = send(m_socket, buf, len, MSG_NOSIGNAL);
			if(result < 0)
				throw SocketException("send() failed: " + Utils::LastError());
		}

		bool Socket::IsConnected()
		{
			return !m_shutdown;
		}

		void Socket::Shutdown()
		{
			m_shutdown = true;
			shutdown(m_socket, SHUT_RDWR);
		}

		void Socket::Close()
		{
			Shutdown();
			close(m_socket);
		}

		Socket::~Socket()
		{
			if(m_server)
				delete m_address;
		}
	}
}
