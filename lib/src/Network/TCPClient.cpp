#include <SystemX/Network/TCPClient.hpp>

namespace SystemX
{
	namespace Network
	{
		TCPClient::TCPClient()
			: Socket(ProtocolFamily::IPv4, SocketType::Stream, 0)
		{
		}

		TCPClient::TCPClient(int socket, InetAddress *address, SocketServer<Socket> *server)
			: Socket(socket, address, server)
		{
		}

		TCPClient::~TCPClient()
		{
		}
	}
}
