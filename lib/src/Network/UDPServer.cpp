#include <SystemX/Network/UDPServer.hpp>

namespace SystemX
{
	namespace Network
	{
		UDPServer::UDPServer(InetAddress address)
			: SocketServer<>(static_cast<ProtocolFamily>(address.GetAddressFamily()), SocketType::Datagram, ProtocolType::UDP)
		{
			SocketServer<>::Bind(address);
		}

		UDPServer::UDPServer(ProtocolFamily family)
			: SocketServer<>(family, SocketType::Datagram, ProtocolType::UDP)
		{
		}

		ssize_t UDPServer::RecvFrom(void *buffer, InetAddress &address)
		{
			return SocketServer<>::RecvFrom(buffer, address);
		}

		ssize_t UDPServer::RecvFrom(void *buffer, std::size_t length, InetAddress &address)
		{
			return SocketServer<>::RecvFrom(buffer, length, address);
		}

		ssize_t UDPServer::SendTo(const void *buffer, InetAddress address)
		{
			return SocketServer<>::SendTo(buffer, address);
		}

		ssize_t UDPServer::SendTo(const void *buffer, std::size_t length, InetAddress address)
		{
			return SocketServer<>::SendTo(buffer, length, address);
		}

		bool UDPServer::IsShutdown()
		{
			return SocketServer<>::IsShutdown();
		}

		void UDPServer::Shutdown()
		{
			SocketServer<>::Shutdown();
		}

		void UDPServer::Close()
		{
			SocketServer<>::Close();
		}
	}
}
