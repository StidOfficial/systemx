#include <SystemX/Network/URI.hpp>

#include <SystemX/Utils.hpp>

namespace SystemX
{
	namespace Network
	{
		URI::URI()
		{
		}

		URI::URI(std::string url)
		{
			std::size_t pos, start_pos = 0, end_pos;

			end_pos = url.find(":");
			m_scheme = url.substr(start_pos, end_pos);
			start_pos = url.find("//", end_pos);
			end_pos = url.find("/", start_pos + 2);
			if(end_pos == std::string::npos)
				m_authority = url.substr(start_pos + 2);
			else
			{
				m_authority = url.substr(start_pos + 2, end_pos - (start_pos + 2));

				pos = m_authority.find(":");
				if(pos == std::string::npos)
				{
					m_host = m_authority;
					if(m_scheme == "http")
						m_port = 80;
					else if(m_scheme == "https")
						m_port = 443;
				}
				else
				{
					m_host = m_authority.substr(0, pos);
					m_port = std::stoi(m_authority.substr(pos + 1, end_pos - (pos + 1)));
				}

				start_pos = end_pos;
				end_pos = url.find("?", start_pos + 1);

				if(end_pos == std::string::npos)
					m_path = url.substr(start_pos);
				else
				{
					m_path = url.substr(start_pos, end_pos - start_pos);

					start_pos = end_pos;
					end_pos = url.find("#", start_pos + 1);

					if(end_pos == std::string::npos)
						parseQuery(url.substr(start_pos + 1));
					else
					{
						parseQuery(url.substr(start_pos + 1, end_pos - (start_pos + 1)));
						m_fragment = url.substr(end_pos + 1);
					}
				}
			}
		}

		void URI::parseQuery(std::string query)
		{
			std::string str = query;
			std::size_t pos;
			do {
					pos = str.find("&");

					std::vector<std::string> key_value = SystemX::Utils::Split(str.substr(0, pos), "=", 2);

					m_query.emplace(key_value[0], key_value[1]);

					str = str.substr(pos + 1);
			} while(pos != std::string::npos);
		}

		std::string URI::Scheme()
		{
			return m_scheme;
		}

		std::string URI::Host()
		{
			return m_host;
		}

		int URI::Port()
		{
			return m_port;
		}

		std::string URI::Path()
		{
			return m_path;
		}

		std::string URI::AbsolutePath()
		{
			std::string query = QueryToString();
			return Path() + ((query.empty()) ? "" : "?" + query);
		}

		std::map<std::string, std::string> URI::Query()
		{
			return m_query;
		}

		std::string URI::QueryToString()
		{
			std::string query;

			for(std::map<std::string, std::string>::iterator it = m_query.begin(); it != m_query.end(); it++)
				query += it->first + "=" + it->second + ((std::next(it) != m_query.end()) ? "&" : "");

			return query;
		}
	}
}
