#include <SystemX/Plugins.hpp>

#include <SystemX/Logger.hpp>

#include <dlfcn.h>
#include <experimental/filesystem>

namespace SystemX
{
	std::vector<void*> Plugins::m_handlers;
	std::map<std::string, Plugin*> Plugins::m_plugins;

	void Plugins::Initialize()
	{
		std::string path = "plugins";
		if(std::experimental::filesystem::exists(path))
			for(auto& p : std::experimental::filesystem::directory_iterator(path))
			{
				if(p.path().extension() != ".so")
					continue;

				SystemX::Logger::Debug("Plugin %s found", p.path().filename().c_str());
				void *handler = dlopen(p.path().c_str(), RTLD_LAZY);
				if(!handler)
				{
					SystemX::Logger::Error("Failed to load plugin : %s", dlerror());
					continue;
				}

				void (*plugin_register)();
				plugin_register = (void (*)())dlsym(handler, "plugin_register");
				if(!plugin_register)
				{
					dlclose(handler);
					SystemX::Logger::Debug("Failed to call sym : %s", dlerror());
					continue;
				}

				m_handlers.push_back(handler);

				plugin_register();
				SystemX::Logger::Debug("Plugin loaded !");
			}

		SystemX::Logger::Info("Plugins loaded %d, registered %d", m_handlers.size(), m_plugins.size());
	}

	void Plugins::Register(Plugin *plugin)
	{
		m_plugins.insert(std::pair<std::string, Plugin*>(plugin->Name(), plugin));
		SystemX::Logger::Info("Plugin %s registered !", plugin->Name().c_str());
	}

	std::map<std::string, Plugin*> Plugins::All()
	{
		return m_plugins;
	}

	void Plugins::Destroy()
	{
		for(const auto& handler : m_handlers)
			dlclose(handler);

		SystemX::Logger::Debug("Plugins destroy !");
	}
}
