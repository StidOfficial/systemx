#include <SystemX/SMBIOS/BIOSInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		BIOSInformation::BIOSInformation(std::string vendor, std::string version, uint16_t startingAddressSegment, std::string releaseDate,
							uint8_t ROMSize, uint64_t characteristics, uint8_t characteristicsExtensionByte1,
							uint8_t characteristicsExtensionByte2, uint8_t systemBIOSMajorRelease, uint8_t systemBIOSMinorRelease,
							uint8_t embeddedControllerFirmwareMajorRelease, uint8_t embeddedControllerFirmwareMinorRelease,
							uint16_t extendedBIOSROMSize)
			: m_vendor(vendor), m_version(version), m_startingAddressSegment(startingAddressSegment), m_releaseDate(releaseDate),
				m_ROMSize(ROMSize), m_characteristics(characteristics), m_characteristicsExtensionByte1(characteristicsExtensionByte1),
				m_characteristicsExtensionByte2(characteristicsExtensionByte2), m_systemBIOSMajorRelease(systemBIOSMajorRelease),
				m_systemBIOSMinorRelease(systemBIOSMinorRelease), m_embeddedControllerFirmwareMajorRelease(embeddedControllerFirmwareMajorRelease),
				m_embeddedControllerFirmwareMinorRelease(embeddedControllerFirmwareMinorRelease), m_extendedBIOSROMSize(extendedBIOSROMSize)
		{
		}

		std::string BIOSInformation::Vendor()
		{
			return m_vendor;
		}

		std::string BIOSInformation::Version()
		{
			return m_version;
		}

		uint16_t BIOSInformation::StartingAddressSegment()
		{
			return m_startingAddressSegment;
		}

		std::string BIOSInformation::ReleaseDate()
		{
			return m_releaseDate;
		}

		uint8_t BIOSInformation::ROMSize()
		{
			return m_ROMSize;
		}

		bool BIOSInformation::Characteristics(uint8_t bit)
		{
			return m_characteristics & (1 << bit);
		}

		bool BIOSInformation::CharacteristicsExtensionByte1(uint8_t bit)
		{
			return m_characteristicsExtensionByte1 & (1 << bit);
		}

		bool BIOSInformation::CharacteristicsExtensionByte2(uint8_t bit)
		{
			return m_characteristicsExtensionByte2 & (1 << bit);
		}

		uint8_t BIOSInformation::SystemBIOSMajorRelease()
		{
			return m_systemBIOSMajorRelease;
		}

		uint8_t BIOSInformation::SystemBIOSMinorRelease()
		{
			return m_systemBIOSMinorRelease;
		}

		uint8_t BIOSInformation::EmbeddedControllerFirmwareMajorRelease()
		{
			return m_embeddedControllerFirmwareMajorRelease;
		}

		uint8_t BIOSInformation::EmbeddedControllerFirmwareMinorRelease()
		{
			return m_embeddedControllerFirmwareMinorRelease;
		}

		bool BIOSInformation::BIOSCharacteristicsNotSupported()
		{
			return Characteristics(3);
		}

		bool BIOSInformation::ISASupported()
		{
			return Characteristics(4);
		}

		bool BIOSInformation::MCASupported()
		{
			return Characteristics(5);
		}

		bool BIOSInformation::EISASupported()
		{
			return Characteristics(6);
		}

		bool BIOSInformation::PCISupported()
		{
			return Characteristics(7);
		}

		bool BIOSInformation::PCMCIASupported()
		{
			return Characteristics(8);
		}

		bool BIOSInformation::PlugAndPlaySupported()
		{
			return Characteristics(9);
		}

		bool BIOSInformation::APMSupported()
		{
			return Characteristics(10);
		}

		bool BIOSInformation::BIOSUpgradable()
		{
			return Characteristics(11);
		}

		bool BIOSInformation::BIOSShadowingAllowed()
		{
			return Characteristics(12);
		}

		bool BIOSInformation::VLVESASupported()
		{
			return Characteristics(13);
		}

		bool BIOSInformation::ESCDAvailable()
		{
			return Characteristics(14);
		}

		bool BIOSInformation::BootFromCDSupported()
		{
			return Characteristics(15);
		}

		bool BIOSInformation::SelectableBootSupported()
		{
			return Characteristics(16);
		}

		bool BIOSInformation::BIOSROMSocketed()
		{
			return Characteristics(17);
		}

		bool BIOSInformation::BootFromPCMCIASupported()
		{
			return Characteristics(18);
		}

		bool BIOSInformation::EDDSpecificationSupported()
		{
			return Characteristics(19);
		}

		bool BIOSInformation::ACPISupported()
		{
			return CharacteristicsExtensionByte1(0);
		}

		bool BIOSInformation::USBLegacySupported()
		{
			return CharacteristicsExtensionByte1(1);
		}

		bool BIOSInformation::AGPSupported()
		{
			return CharacteristicsExtensionByte1(2);
		}

		bool BIOSInformation::I2OBootSupported()
		{
			return CharacteristicsExtensionByte1(3);
		}

		bool BIOSInformation::LS120BootSupported()
		{
			return CharacteristicsExtensionByte1(4);
		}

		bool BIOSInformation::ATAPIZIPDriveBootSupported()
		{
			return CharacteristicsExtensionByte1(5);
		}

		bool BIOSInformation::SmartBatterySupported()
		{
			return CharacteristicsExtensionByte1(7);
		}

		bool BIOSInformation::BIOSBootSpecficiationSupported()
		{
			return CharacteristicsExtensionByte2(0);
		}

		bool BIOSInformation::TargetedContentDistribution()
		{
			return CharacteristicsExtensionByte2(2);
		}

		bool BIOSInformation::UEFISupported()
		{
			return CharacteristicsExtensionByte2(3);
		}
	}
}
