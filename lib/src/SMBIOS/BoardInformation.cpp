#include <SystemX/SMBIOS/BoardInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		BoardInformation::BoardInformation(std::string manufacturer, std::string product, std::string version,
						std::string serialNumber, std::string assetTag, uint8_t featureFlags,
						std::string locationInChassis, uint16_t chassisHandle, BoardType boardType,
						std::vector<uint16_t> containedObjectHandles)
			: m_manufacturer(manufacturer), m_product(product), m_version(version),
				m_serialNumber(serialNumber), m_assetTag(assetTag), m_featureFlags(featureFlags),
				m_locationInChassis(locationInChassis), m_chassisHandle(chassisHandle), m_boardType(boardType),
				m_containedObjectHandles(containedObjectHandles)
		{
		}

		std::string BoardInformation::Manufacturer()
		{
			return m_manufacturer;
		}

		std::string BoardInformation::Product()
		{
			return m_product;
		}

		std::string BoardInformation::Version()
		{
			return m_version;
		}

		std::string BoardInformation::SerialNumber()
		{
			return m_serialNumber;
		}

		std::string BoardInformation::AssetTag()
		{
			return m_assetTag;
		}

		bool BoardInformation::FeatureFlag(uint8_t bit)
		{
			return m_featureFlags & (1 << bit);
		}

		bool BoardInformation::IsHotSwappable()
		{
			return FeatureFlag(4);
		}

		bool BoardInformation::IsReplaceable()
		{
			return FeatureFlag(3);
		}

		bool BoardInformation::IsRemovable()
		{
			return FeatureFlag(2);
		}

		bool BoardInformation::RequiresAtLeastOneDaughterBoard()
		{
			return FeatureFlag(1);
		}

		bool BoardInformation::IsHostingBoard()
		{
			return FeatureFlag(0);
		}

		std::string BoardInformation::LocationInChassis()
		{
			return m_locationInChassis;
		}

		uint16_t BoardInformation::ChassisHandle()
		{
			return m_chassisHandle;
		}

		BoardInformation::BoardType BoardInformation::GetBoardType()
		{
			return m_boardType;
		}

		std::vector<uint16_t> BoardInformation::ContainedObjectHandles()
		{
			return m_containedObjectHandles;
		}
	}
}
