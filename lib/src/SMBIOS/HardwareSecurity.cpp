#include <SystemX/SMBIOS/HardwareSecurity.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		HardwareSecurity::HardwareSecurity(uint8_t hardwareSecuritySettings)
			: m_hardwareSecuritySettings(hardwareSecuritySettings)
		{
		}
	}
}
