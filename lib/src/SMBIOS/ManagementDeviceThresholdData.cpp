#include <SystemX/SMBIOS/ManagementDeviceThresholdData.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		ManagementDeviceThresholdData::ManagementDeviceThresholdData(uint16_t lowerThresholdNonCritical, uint16_t upperThresholdNonCritical,
										uint16_t lowerThresholdCritical, uint16_t upperThresholdCritical,
										uint16_t lowerThresholdNonRecoverable, uint16_t upperThresholdNonRecoverable)
			: m_lowerThresholdNonCritical(lowerThresholdNonCritical), m_upperThresholdNonCritical(upperThresholdNonCritical),
				m_lowerThresholdCritical(lowerThresholdCritical), m_upperThresholdCritical(upperThresholdCritical),
				m_lowerThresholdNonRecoverable(lowerThresholdNonRecoverable), m_upperThresholdNonRecoverable(upperThresholdNonRecoverable)
		{
		}

		uint16_t ManagementDeviceThresholdData::LowerThresholdNonCritical()
		{
			return m_lowerThresholdNonCritical;
		}

		uint16_t ManagementDeviceThresholdData::UpperThresholdNonCritical()
		{
			return m_upperThresholdNonCritical;
		}

		uint16_t ManagementDeviceThresholdData::LowerThresholdCritical()
		{
			return m_lowerThresholdCritical;
		}

		uint16_t ManagementDeviceThresholdData::UpperThresholdCritical()
		{
			return m_upperThresholdCritical;
		}

		uint16_t ManagementDeviceThresholdData::LowerThresholdNonRecoverable()
		{
			return m_lowerThresholdNonRecoverable;
		}

		uint16_t ManagementDeviceThresholdData::UpperThresholdNonRecoverable()
		{
			return m_upperThresholdNonRecoverable;
		}
	}
}
