#include <SystemX/SMBIOS/OnBoardDeviceInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		OnBoardDeviceInformation::OnBoardDeviceInformation(uint8_t type, std::string description)
			: m_type(type), m_description(description)
		{
		}

		bool OnBoardDeviceInformation::Status()
		{
			return m_type & (1 << 7);
		}

		OnBoardDeviceInformation::Type OnBoardDeviceInformation::GetType()
		{
			return static_cast<Type>(m_type &= ~0 ^ (1 << 7));
		}

		std::string OnBoardDeviceInformation::Description()
		{
			return m_description;
		}
	}
}
