#include <SystemX/SMBIOS/SystemBootInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		SystemBootInformation::SystemBootInformation(uint8_t *reserved, uint8_t *bootStatus)
			: m_reserved(reserved), m_bootStatus(bootStatus)
		{
		}

		uint8_t *SystemBootInformation::Reserved()
		{
			return m_reserved;
		}

		uint8_t *SystemBootInformation::BootStatus()
		{
			return m_bootStatus;
		}
	}
}
