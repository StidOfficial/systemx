#include <SystemX/SMBIOS/SystemEnclosureChassisInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		SystemEnclosureChassisInformation::SystemEnclosureChassisInformation(std::string manufacturer, Type type, std::string version, std::string serialNumber, std::string assetTag,
								State bootUpState, State powerSupplyState, State thermalState, Status securityState, uint32_t OEMdefined,
								uint8_t height, uint8_t numberOfPowerCords, std::vector<uint8_t> containedElements, std::string SKUNumber)
			: m_manufacturer(manufacturer), m_type(type), m_version(version), m_serialNumber(serialNumber), m_assetTag(assetTag), m_bootUpState(bootUpState),
				m_powerSupplyState(powerSupplyState), m_thermalState(thermalState), m_securityState(securityState), m_OEMdefined(OEMdefined),
				m_height(height), m_numberOfPowerCords(numberOfPowerCords), m_containedElements(containedElements), m_SKUNumber(SKUNumber)
		{
		}

		std::string SystemEnclosureChassisInformation::Manufacturer()
		{
			return m_manufacturer;
		}

		bool SystemEnclosureChassisInformation::IsLocked()
		{
			return static_cast<uint8_t>(m_type) & (1 << 7);
		}

		SystemEnclosureChassisInformation::Type SystemEnclosureChassisInformation::GetType()
		{
			return m_type;
		}

		std::string SystemEnclosureChassisInformation::Version()
		{
			return m_version;
		}

		std::string SystemEnclosureChassisInformation::SerialNumber()
		{
			return m_serialNumber;
		}

		std::string SystemEnclosureChassisInformation::AssetTag()
		{
			return m_assetTag;
		}

		SystemEnclosureChassisInformation::State SystemEnclosureChassisInformation::BootUp()
		{
			return m_bootUpState;
		}

		SystemEnclosureChassisInformation::State SystemEnclosureChassisInformation::PowerSupply()
		{
			return m_powerSupplyState;
		}

		SystemEnclosureChassisInformation::State SystemEnclosureChassisInformation::Thermal()
		{
			return m_thermalState;
		}

		SystemEnclosureChassisInformation::Status SystemEnclosureChassisInformation::SecurityState()
		{
			return m_securityState;
		}

		uint32_t SystemEnclosureChassisInformation::OEMDefined()
		{
			return m_OEMdefined;
		}

		uint8_t SystemEnclosureChassisInformation::Height()
		{
			return m_height;
		}

		uint8_t SystemEnclosureChassisInformation::NumberOfPowerCords()
		{
			return m_numberOfPowerCords;
		}

		std::vector<uint8_t> SystemEnclosureChassisInformation::ContainedElements()
		{
			return m_containedElements;
		}

		std::string SystemEnclosureChassisInformation::SKUNumber()
		{
			return m_SKUNumber;
		}
	}
}
