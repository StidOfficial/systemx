#include "sah.hpp"

#include <SystemX/Network/HTTP/Client.hpp>
#include <SystemX/Network/HTTP/Response.hpp>

#include <algorithm>

#include <SystemX/Logger.hpp> // Tmp

Sah::Sah(std::string address)
	: m_address(address), m_cookies(new SystemX::Network::HTTP::Cookies())
{
}

Json::Value Sah::Execute(SystemX::Network::HTTP::Method method, std::string path, std::string data)
{
	return Execute(method, path, std::vector<char>(data.begin(), data.end()));
}

Json::Value Sah::Execute(SystemX::Network::HTTP::Method method, std::string path, std::vector<char> data)
{
	Json::Value root;

	try
	{
		SystemX::Network::HTTP::Client client = SystemX::Network::HTTP::Client();

		SystemX::Network::HTTP::Headers headers;
		if(!m_contextID.empty())
			headers.Add("X-Context", m_contextID);

		headers.Add("X-Prototype-Version", "1.7");
		headers.Add("X-Requested-With", "XMLHttpRequest");
		headers.Add("X-Sah-Request-Type", "idle");

		headers.Set("Content-type", "application/x-sah-ws-1-call+json; charset=UTF-8");

		std::replace(path.begin(), path.end(), '.', '/');

		std::string url = "http://" + m_address + "/" + path;
		SystemX::Logger::Debug("Sah URI : %s", url.c_str());

		SystemX::Network::HTTP::Response *response;
		if(method == SystemX::Network::HTTP::Method::POST)
			response = client.Post(SystemX::Network::URI(url), headers, m_cookies, data);
		else if(method == SystemX::Network::HTTP::Method::GET)
			response = client.Get(SystemX::Network::URI(url), headers, m_cookies);
		else
			throw SahException("Method not supported");

		if(response->GetStatusCode() == 403)
			throw SahException("Access forbidden");

		std::string jsonText = response->String();

		delete response;

		Json::CharReaderBuilder builder;
		Json::CharReader *reader = builder.newCharReader();

		std::string errors;

		bool parsed = reader->parse(jsonText.c_str(), jsonText.c_str() + jsonText.size(), &root, &errors);
		delete reader;

		if(!parsed)
			SystemX::Logger::Error("Sah JSON parse failed : %s", errors.c_str());
	}
	catch(SahException &e)
	{
		SystemX::Logger::Error("Failed Sah : %s", e.what());
	}

	return root;
}

bool Sah::Authenticate(std::string username, std::string password)
{
	Json::Value root = Execute(SystemX::Network::HTTP::Method::POST, "authenticate?username=" + username + "&password=" + password);
	m_contextID = root["data"]["contextID"].asString();

	return !m_contextID.empty();
}

Json::Value Sah::Dsl()
{
	return Execute(SystemX::Network::HTTP::Method::POST, "sysbus.NeMo.Intf.data:getMIBs", "{\"parameters\":{\"mibs\":\"dsl\",\"flag\":\"\",\"traverse\":\"down\"}}");
}

Json::Value Sah::WANStatus()
{
	return Execute(SystemX::Network::HTTP::Method::POST, "sysbus.NMC:getWANStatus");
}

Json::Value Sah::IPTVStatus()
{
	return Execute(SystemX::Network::HTTP::Method::POST, "sysbus.NMC.OrangeTV:getIPTVStatus");
}

Json::Value Sah::DeviceInfo()
{
	return Execute(SystemX::Network::HTTP::Method::GET, "sysbus.DeviceInfo");
}

Json::Value Sah::PhoneTrunk()
{
	return Execute(SystemX::Network::HTTP::Method::POST, "sysbus.VoiceService.VoiceApplication:listTrunks");
}

Json::Value Sah::WifiStats()
{
	return Execute(SystemX::Network::HTTP::Method::POST, "sysbus.NMC.Wifi:getStats");
}

Sah::~Sah()
{
	delete m_cookies;
}
