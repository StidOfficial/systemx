#include "livebox.hpp"

#include <SystemX/Network/HTTP/Client.hpp>
#include <SystemX/Network/HTTP/Response.hpp>

#include <SystemX/Logger.hpp>

LiveboxDriver::LiveboxDriver()
	: SystemX::Driver("livebox", "1.0.0.2019")
{
}

void LiveboxDriver::Initialize()
{
	SystemX::Logger::Debug("livebox initialized !");
}

void LiveboxDriver::Initialize(SystemX::Equipment::Equipment *equipment)
{
	SystemX::Logger::Debug("livebox initialized for %s !", equipment->GetName().c_str());
}

void LiveboxDriver::Uninitialize(SystemX::Equipment::Equipment *equipment)
{
	SystemX::Logger::Debug("livebox uninitialized for %s !", equipment->GetName().c_str());
}

void LiveboxDriver::Uninitialize()
{
	SystemX::Logger::Debug("livebox uninitialized !");
}
