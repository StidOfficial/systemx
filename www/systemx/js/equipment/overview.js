const equipmentOverview = {
	data: function() {
		return {
			name: "SERVER-1",
			status: true,
			images: [
				"https://www.stalliontek.com/media/catalog/product/cache/1/thumbnail/600x/17f82f742ffe127f42dca9de82fb58b1/r/6/r610_1_8.jpg",
				"https://www.stalliontek.com/media/catalog/product/cache/1/thumbnail/600x/17f82f742ffe127f42dca9de82fb58b1/r/6/r610_2_1_3.jpg",
				"https://www.stalliontek.com/media/catalog/product/cache/1/thumbnail/600x/17f82f742ffe127f42dca9de82fb58b1/r/6/r610_3_8.jpg"
			],
			manufacturer: "Dell",
			productName: "PowerEdge R610"			
		}
	},
	mounted: function() {
		var cpuCtx = this.$refs.cpuChart.getContext('2d');
		var cpuChart = new Chart(cpuCtx, {
			type: 'line',
			data: {
				labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
				datasets: [
					{
						label: 'CPU1',
						data: [100, 19, 3, 5, 2, 3],
						backgroundColor: randomColor(),
						borderWidth: 1
					},
					{
						label: 'CPU2',
						data: [50, 25, 70, 20, 10, 5],
						backgroundColor: randomColor(),
						borderWidth: 1
					},
				]
			},
			options: {
				title: {
					display: true,
					position: "left",
					text: "CPU Usage"
				},
				scales: {
					responsive: true,
					yAxes: [{
						ticks: {
							beginAtZero: true,
							callback: function(value, index, values) {
								return value + " %";
							}
						}
					}]
				}
			}
		});

		var memoryCtx = this.$refs.memoryChart.getContext('2d');
		var memoryChart = new Chart(memoryCtx, {
			type: 'line',
			data: {
				labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
				datasets: [
					{
						label: 'Memory',
						data: [100, 19, 3, 5, 2, 3],
						backgroundColor: randomColor(),
						borderWidth: 1
					},
					{
						label: 'Swap',
						data: [50, 25, 70, 20, 10, 5],
						backgroundColor: randomColor(),
						borderWidth: 1
					},
				]
			},
			options: {
				title: {
					display: true,
					position: "left",
					text: "Memory Usage"
				},
				scales: {
					responsive: true,
					yAxes: [{
						ticks: {
							beginAtZero: true,
							callback: function(value, index, values) {
								return value + " %";
							}
						}
					}]
				}
			}
		});

		var networkCtx = this.$refs.networkChart.getContext('2d');
		var networkChart = new Chart(networkCtx, {
			type: 'line',
			data: {
				labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
				datasets: [
					{
						label: 'Recieve',
						data: [100, 19, 3, 5, 2, 3],
						backgroundColor: randomColor(),
						borderColor: randomColor(),
						fill: false,
						borderWidth: 1
					},
					{
						label: 'Send',
						data: [50, 25, 70, 20, 10, 5],
						backgroundColor: randomColor(),
						borderColor: randomColor(),
						fill: false,
						borderWidth: 1
					},
				]
			},
			options: {
				title: {
					display: true,
					position: "left",
					text: "Network Usage"
				},
				scales: {
					responsive: true,
					yAxes: [{
						ticks: {
							beginAtZero: true,
							callback: function(value, index, values) {
								return value + " %";
							}
						}
					}]
				}
			}
		});

		var diskCtx = this.$refs.diskChart.getContext('2d');
		var diskChart = new Chart(diskCtx, {
			type: 'line',
			data: {
				labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
				datasets: [
					{
						label: 'Read',
						data: [100, 19, 3, 5, 2, 3],
						backgroundColor: randomColor(),
						borderColor: randomColor(),
						fill: false,
						borderWidth: 1
					},
					{
						label: 'Write',
						data: [50, 25, 70, 20, 10, 5],
						backgroundColor: randomColor(),
						borderColor: randomColor(),
						fill: false,
						borderWidth: 1
					},
				]
			},
			options: {
				title: {
					display: true,
					position: "left",
					text: "Disk Usage"
				},
				scales: {
					responsive: true,
					yAxes: [{
						ticks: {
							beginAtZero: true,
							callback: function(value, index, values) {
								return value + " %";
							}
						}
					}]
				}
			}
		});
	},
	template: `
		<main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 class="h2">
					<i class="fas fa-circle" :class="{'text-success': status, 'text-danger': !status}"></i>
					{{ name }}
				</h1>
			</div>

			<div class="card bg-light mb-3">
				<div class="row no-gutters">
					<div class="col-md-4">
						<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
								<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item" v-for="(image, index) in images" :class="{'active': index === 0}">
									<img :src="image" class="d-block w-100" alt="...">
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<div class="col-md-8">
						<div class="card-body">
							<h5 class="card-title">{{ productName }} ({{ manufacturer }})</h5>
							<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
							<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
						</div>
					</div>
				</div>
			</div>

			<div class="card-group mb-3">
				<div class="card">
					<div class="card-header"><i class="fas fa-microchip"></i> CPU</div>
					<div class="card-body">
						<canvas ref="cpuChart" width="400" height="150"></canvas>
					</div>
				</div>
				<div class="card">
					<div class="card-header"><i class="fas fa-memory"></i> Memory</div>
					<div class="card-body">
						<canvas ref="memoryChart" width="400" height="150"></canvas>
					</div>
				</div>
			</div>

			<div class="card-group mb-3">
				<div class="card">
					<div class="card-header"><i class="fas fa-network-wired"></i> Network I/O</div>
					<div class="card-body">
						<canvas ref="networkChart" width="400" height="150"></canvas>
					</div>
				</div>
				<div class="card">
					<div class="card-header"><i class="fas fa-hdd"></i> Disk I/O</div>
					<div class="card-body">
						<canvas ref="diskChart" width="400" height="150"></canvas>
					</div>
				</div>
			</div>
		</main>
	`
}
