const sidebar = {
	props: ["name"],
	template: `
		<nav class="col-md-2 d-none d-md-block bg-light sidebar">
			<div class="sidebar-sticky" v-if="$route.name === 'home' || $route.name === 'plugins' || $route.name === 'drivers'">
				<ul class="nav flex-column">
					<li class="nav-item">
						<router-link class="nav-link" :to="{name: 'home'}"><i class="fas fa-home"></i> Home</router-link>
					</li>
					<li class="nav-item">
						<router-link class="nav-link" :to="{name: 'plugins'}"><i class="fas fa-puzzle-piece"></i> Plugins</router-link>
					</li>
					<li class="nav-item">
						<router-link class="nav-link" :to="{name: 'drivers'}"><i class="fas fa-hdd"></i> Drivers</router-link>
					</li>
				</ul>
			</div>

			<div class="sidebar-sticky" v-if="$route.name === 'equipmentOverview' || $route.name === 'equipmentInformations' || $route.name === 'equipmentSMBIOS'">
				<form class="search">
					<input class="form-control" type="text" placeholder="Search">
				</form>

				<ul class="nav flex-column">
					<li class="nav-item">
						<router-link class="nav-link" :to="{name: 'equipmentOverview'}"><i class="fas fa-home"></i> Overview</router-link>
					</li>
					<li class="nav-item">
						<router-link class="nav-link" :to="{name: 'equipmentInformations'}"><i class="fas fa-info-circle"></i> Informations</router-link>
					</li>
					<li class="nav-item">
						<router-link class="nav-link" :to="{name: 'equipmentSMBIOS'}"><i class="fas fa-microchip"></i> SMBIOS</router-link>
					</li>
				</ul>
			</div>
		</nav>
	`
}
