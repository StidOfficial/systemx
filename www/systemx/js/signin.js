const formSignin = {
	data: function() {
		return {
			host: "localhost",
			username: "",
			password: ""
		}
	},
	methods: {
		login() {
			$("#loginModal input#host").attr("disabled", true)
			$("#loginModal input#username").attr("disabled", true)
			$("#loginModal input#password").attr("disabled", true)

			this.$http.post("/api/authentication").then(response => {
				$("#loginModal div[role=alert]").addClass("d-none hide")
				$("#loginModal div[role=alert]").removeClass("show")

				$("#loginModal").modal("hide")
			}, response => {
				$("#loginModal div[role=alert]").html("Invalid username or password")
				$("#loginModal div[role=alert]").removeClass("d-none hide")
				$("#loginModal div[role=alert]").addClass("show")
			})

			$("#loginModal input#host").removeAttr("disabled", true)
			$("#loginModal input#username").removeAttr("disabled", true)
			$("#loginModal input#password").removeAttr("disabled", true)
		}
	},
	template: `
		<form @submit.prevent="login">
			<div class="modal-header">
				<h5 class="modal-title">Sign in</h5>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger fade hide d-none" role="alert"></div>

				<div class="form-group">
					<label for="username" class="col-form-label">Host :</label>
					<input type="text" class="form-control" id="host" v-model="host">
				</div>
				<div class="form-group">
					<label for="username" class="col-form-label">Username :</label>
					<input type="text" class="form-control" id="username" v-model="username" autofocus>
				</div>
				<div class="form-group">
					<label for="password" class="col-form-label">Password :</label>
					<input type="password" class="form-control" id="password" v-model="password">
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">
					<span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="false"></span>
					<i class="fas fa-sign-in-alt"></i> Sign in
				</button>
			</div>
		</form>
	`,
	mounted: function() {
		this.$http.get("/api/authenticated").then(response => {
			if(response.body.isAuthenticated)
				$("#loginModal").modal("hide")
		})
	}
}
